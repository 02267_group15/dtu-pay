/** @author Nicolai */
package dtu.group15;

import dtu.group15.events.inputs.*;
import dtu.group15.events.outputs.TokenNotValidatedEvent;
import dtu.group15.events.outputs.TokenValidatedEvent;
import dtu.group15.events.outputs.TokensIssuedEvent;
import dtu.group15.events.outputs.TokensNotIssuedEvent;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TokenTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void validateTokenSuccessfullyTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        // Issue tokens
        Id customerID = new Id("Test customer");
        Amount tokenAmount = new Amount(5);
        Event issueTokenEvent = new IssueTokensRequestedEvent(correlationId, customerID, tokenAmount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent, IssueTokensRequestedEvent.class);

        // Customer exists
        Event customerExistedEvent = new CustomerExistedEvent(correlationId, customerID).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistedEvent, CustomerExistedEvent.class);

        // Tokens issued
        Event capturedEvent = capturedEvents.removeFirst();
        assertNotNull(capturedEvent);
        List<Token> tokens = capturedEvent.getArgument(0, TokensIssuedEvent.class).getTokenList().getTokenList();
        assertEquals(5, tokens.size());

        // Validate tokens
        Token token = tokens.getFirst();
        Event paymentRequestedEvent = new PaymentRequestedEvent(correlationId, null, token, null).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        // Initialize expected output event
        Event expected = new TokenValidatedEvent(correlationId, customerID).asEvent();

        // Tokens validated
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void validateTokenFailedTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        // Issue tokens
        Id customerID = new Id("Test customer");
        Amount tokenAmount = new Amount(5);
        Event issueTokenEvent = new IssueTokensRequestedEvent(correlationId, customerID, tokenAmount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent, IssueTokensRequestedEvent.class);

        // Customer exists
        Event customerExistedEvent = new CustomerExistedEvent(correlationId, customerID).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistedEvent, CustomerExistedEvent.class);

        // Tokens issued
        assertNotNull(capturedEvents.removeFirst());

        // Validate wrong token
        Token token = new Token("RANDOM TOKEN");
        Event paymentRequestedEvent = new PaymentRequestedEvent(correlationId, null, token, null).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        // Initialize expected output event
        Event expected = new TokenNotValidatedEvent(correlationId, "Token is not valid").asEvent();

        // Token not validated
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void wrongNumberOfTokensRequestedTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        // Issue tokens
        Id customerID = new Id("Test customer");
        Amount tokenAmount = new Amount(6);
        Event issueTokenEvent = new IssueTokensRequestedEvent(correlationId, customerID, tokenAmount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent, IssueTokensRequestedEvent.class);

        // Customer existed
        Event customerExistsEvent = new CustomerExistedEvent(correlationId, customerID).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistsEvent, CustomerExistedEvent.class);

        // Initialize expected output event
        Event expected = new TokensNotIssuedEvent(correlationId, "Amount of tokens requested must be between 1 and 5").asEvent();

        // Tokens not issued
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void alreadyHasTokensTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        // Issue tokens
        Id customerID = new Id("Test customer");
        Amount tokenAmount = new Amount(2);
        Event issueTokenEvent = new IssueTokensRequestedEvent(correlationId, customerID, tokenAmount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent, IssueTokensRequestedEvent.class);

        // Customer existed
        Event customerExistsEvent = new CustomerExistedEvent(correlationId, customerID).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistsEvent, CustomerExistedEvent.class);
        Event capturedEvent = capturedEvents.removeFirst();
        assertNotNull(capturedEvent);

        // Issue tokens
        Id customerID2 = new Id("Test customer");
        Amount tokenAmount2 = new Amount(2);
        Event issueTokenEvent2 = new IssueTokensRequestedEvent(correlationId, customerID2, tokenAmount2).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent2, IssueTokensRequestedEvent.class);

        // Customer existed
        Event customerExistsEvent2 = new CustomerExistedEvent(correlationId, customerID2).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistsEvent2, CustomerExistedEvent.class);

        // Initialize expected output event
        Event expected = new TokensNotIssuedEvent(correlationId, "Customer cannot request any more tokens").asEvent();

        // Tokens not issued
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void clearTokensTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        // Issue tokens
        Id customerID = new Id("Test customer");
        Amount tokenAmount = new Amount(3);
        Event issueTokenEvent = new IssueTokensRequestedEvent(correlationId, customerID, tokenAmount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent, IssueTokensRequestedEvent.class);

        // Customer existed
        Event customerExistsEvent = new CustomerExistedEvent(correlationId, customerID).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistsEvent, CustomerExistedEvent.class);

        // Tokens issued
        Event capturedEvent = capturedEvents.removeFirst();
        assertNotNull(capturedEvent);
        List<Token> tokens = capturedEvent.getArgument(0, TokensIssuedEvent.class).getTokenList().getTokenList();
        assertEquals(3, tokens.size());

        //Clear tokens after deregistration
        Event deregistrationEvent = new CustomerDeregistrationRequestedEvent(correlationId, new Id("Test customer")).asEvent();
        TestUtil.publishEvent(consumerMap, deregistrationEvent, CustomerDeregistrationRequestedEvent.class);

        // Issue tokens again
        Id customerID2 = new Id("Test customer");
        Amount tokenAmount2 = new Amount(4);
        Event issueTokenEvent2 = new IssueTokensRequestedEvent(correlationId, customerID2, tokenAmount2).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokenEvent2, IssueTokensRequestedEvent.class);

        // Customer existed again
        Event customerExistsEvent2 = new CustomerExistedEvent(correlationId, customerID2).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistsEvent2, CustomerExistedEvent.class);

        // Tokens issued again
        Event capturedEvent2 = capturedEvents.removeFirst();
        assertNotNull(capturedEvent2);
        List<Token> tokens2 = capturedEvent2.getArgument(0, TokensIssuedEvent.class).getTokenList().getTokenList();
        assertEquals(4, tokens2.size());

        assertTrue(capturedEvents.isEmpty());

    }
}
