/** @author Nicolai */
package dtu.group15;

import dtu.group15.adapter.RabbitMqInputPort;
import dtu.group15.adapter.RabbitMqOutputPort;
import dtu.group15.repository.TokenRepository;
import dtu.group15.service.TokenService;
import messaging.Event;
import messaging.MessageQueue;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class TestUtil {

    /* Utility class to help with testing */

    public static Map<String, List<Consumer<Event>>> mapConsumersToTopics(MessageQueue queue) {
        Map<String, List<Consumer<Event>>> consumerMap = new HashMap<>();
        Mockito.doAnswer(invocation -> {
            String topic = invocation.getArgument(0);
            Consumer<Event> consumer = invocation.getArgument(1);
            if (consumerMap.containsKey(topic)) {
                consumerMap.get(topic).add(consumer);
            } else {
                consumerMap.put(topic, new ArrayList<>() {{ add(consumer); }});
            }
            return null;
        }).when(queue).addHandler(Mockito.anyString(), Mockito.any());
        return consumerMap;
    }

    public static void initializeInputOutputPorts(MessageQueue queue) {
        TokenRepository repo = new TokenRepository();
        RabbitMqOutputPort outputPort = new RabbitMqOutputPort(queue);
        TokenService service = new TokenService(repo, outputPort);
        RabbitMqInputPort inputPort = new RabbitMqInputPort(service, queue);
    }

    public static List<Event> initializeEventPuller(MessageQueue queue) {
        List<Event> capturedEvents = new ArrayList<>();
        Mockito.doAnswer(invocation -> {
            Event event = invocation.getArgument(0);
            capturedEvents.add(event);
            return null;
        }).when(queue).publish(Mockito.any());

        return capturedEvents;
    }

    public static void publishEvent(Map<String, List<Consumer<Event>>> consumerMap, Event event, Class c) {
        consumerMap.get(c.getSimpleName()).forEach(consumer -> consumer.accept(event));
    }
}
