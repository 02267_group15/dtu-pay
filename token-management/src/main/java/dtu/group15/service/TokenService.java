/** @author Michael */
package dtu.group15.service;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.model.*;
import dtu.group15.repository.TokenRepository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TokenService {
    private Map<CorrelationId, CompletableTokenRequest> issueTokensCorrelations = new ConcurrentHashMap<>();

    private final TokenRepository tokenRepository;

    private final IOutputPort outputPort;

    public TokenService(TokenRepository tokenRepository, IOutputPort outputPort) {
        this.tokenRepository = tokenRepository;
        this.outputPort = outputPort;
    }

    //Handles an issue tokens request event. Either starts or finishes the process
    public void issueTokens(CorrelationId correlationId, Id customerId, Amount amount) {
        CompletableTokenRequest completableTokenRequest = getCompletableTokenRequest(correlationId);
        completableTokenRequest.setCustomerId(customerId);
        completableTokenRequest.setAmount(amount);
        issueTokenRequestUpdated(correlationId);
    }

    //Validates a token and sends an event
    public void validateToken(CorrelationId correlationId, Token token) {
        try {
            Id customerId = tokenRepository.validateToken(token);
            outputPort.createTokenValidatedEvent(correlationId, customerId);
        } catch (Exception e) {
            outputPort.createTokenNotValidatedEvent(correlationId, e.getMessage());
        }
    }

    //Removes the tokens from a customer
    public void cleanupToken(CorrelationId correlationId, Id customerId) {
        tokenRepository.cleanupTokens(customerId);
    }

    //Customer exists event. Starts or finishes a token issuing process
    public void updateCustomerExists(CorrelationId correlationId, Id customerId) {
        CompletableTokenRequest completableTokenRequest = getCompletableTokenRequest(correlationId);
        completableTokenRequest.setExists(true);
        issueTokenRequestUpdated(correlationId);
    }

    //The issue token process is updated. Finishes the process if it is ready
    public synchronized void issueTokenRequestUpdated(CorrelationId correlationId) {
        CompletableTokenRequest completableTokenRequest = issueTokensCorrelations.get(correlationId);
        if (completableTokenRequest.isComplete()) {
            issueTokensCorrelations.remove(correlationId);
            Id customerId = completableTokenRequest.getCustomerId();
            Amount amount = completableTokenRequest.getAmount();
            finishIssueTokens(correlationId, customerId, amount);
        }
    }

    //Finish an issue tokens process. Sends an event
    public void finishIssueTokens(CorrelationId correlationId, Id customerId, Amount amount) {
        try {
            TokenList tokenList = tokenRepository.issueTokens(customerId, amount);
            outputPort.createTokensIssuedEvent(correlationId, tokenList);
        } catch (Exception e) {
            outputPort.createTokensNotIssuedEvent(correlationId, e.getMessage());
        }
    }

    //Returns the CompletableTokenRequest for a correlation ID. Creates it if it does not already exist
    public synchronized CompletableTokenRequest getCompletableTokenRequest(CorrelationId correlationId) {
        CompletableTokenRequest completableTokenRequest;
        if (!issueTokensCorrelations.containsKey(correlationId)) {
            completableTokenRequest = new CompletableTokenRequest();
            issueTokensCorrelations.put(correlationId, completableTokenRequest);
        }
        else {
            completableTokenRequest = issueTokensCorrelations.get(correlationId);
        }
        return completableTokenRequest;
    }

}
