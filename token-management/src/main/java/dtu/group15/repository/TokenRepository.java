/** @author Magnus */
package dtu.group15.repository;

import dtu.group15.model.Amount;
import dtu.group15.model.Id;
import dtu.group15.model.Token;
import dtu.group15.model.TokenList;

import java.util.*;

public class TokenRepository {
    Map<Id, List<Token>> tokenMap = new HashMap<>();

    //Create a new token and return it
    private Token createToken() {
        Token token = new Token(UUID.randomUUID().toString());
        return token;
    }

    //Issue tokens to a customer. Throws exception if anything goes wrong
    public TokenList issueTokens(Id id, Amount amount) throws IllegalArgumentException {
        List<Token> tokens = tokenMap.get(id);
        List<Token> newTokens = new ArrayList<>();
        if (tokens != null && tokens.size() > 1) {
            throw new IllegalArgumentException("Customer cannot request any more tokens");
        }
        if (amount.getAmount() < 1 || amount.getAmount() > 5) {
            throw new IllegalArgumentException("Amount of tokens requested must be between 1 and 5");
        }
        boolean doesNotExist = tokens == null;
        if (tokens == null) {
            tokens = new ArrayList<>();
        }
        for (int i = 0; i < amount.getAmount(); i++) {
            Token token = createToken();
            tokens.add(token);
            newTokens.add(token);
        }
        if (doesNotExist) {
            tokenMap.put(id, tokens);
        }
        TokenList tokenList = new TokenList();
        tokenList.setTokenList(newTokens);
        return tokenList;
    }

    //Validates a token. Throws an exception if the token is not valid
    public Id validateToken(Token token) throws IllegalArgumentException {
        Set<Id> idSet = tokenMap.keySet();
        Id foundId = null;
        for (Id id : idSet) {
            if (tokenMap.get(id).contains(token)) {
                foundId = id;
                break;
            }
        }
        if (foundId == null) {
            throw new IllegalArgumentException("Token is not valid");
        }
        tokenMap.get(foundId).remove(token);
        return foundId;
    }

    //Removes all tokens from a specific customer
    public void cleanupTokens(Id id) {
        tokenMap.remove(id);
    }
}
