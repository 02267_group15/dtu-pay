package dtu.group15.events.outputs;


import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import lombok.*;
import messaging.Event;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TokenValidatedEvent extends DTUPayEvent {

    private static final long serialVersionUID = 1L;

    CorrelationId correlationId;
    Id customerId;

}
