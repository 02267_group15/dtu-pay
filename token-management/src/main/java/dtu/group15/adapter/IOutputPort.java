/** @author Michael */
package dtu.group15.adapter;

import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import dtu.group15.model.Token;
import dtu.group15.model.TokenList;

public interface IOutputPort {

    void createTokensIssuedEvent(CorrelationId correlationId, TokenList tokenList);

    void createTokensNotIssuedEvent(CorrelationId correlationId, String errorMessage);

    void createTokenValidatedEvent(CorrelationId correlationId, Id customerId);

    void createTokenNotValidatedEvent(CorrelationId correlationId, String errorMessage);

}
