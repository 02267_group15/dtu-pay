/** @author Michael */
package dtu.group15.adapter;

import dtu.group15.events.outputs.*;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import dtu.group15.model.TokenList;
import messaging.Event;
import messaging.MessageQueue;

public class RabbitMqOutputPort implements IOutputPort {

    private final MessageQueue messageQueue;

    public RabbitMqOutputPort(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    /* The methods used in this class are used to publish different events to the message queue */

    @Override
    public void createTokensIssuedEvent(CorrelationId correlationId, TokenList tokenList) {
        Event event = new TokensIssuedEvent(correlationId, tokenList).asEvent();
        messageQueue.publish(event);
    }

    @Override
    public void createTokensNotIssuedEvent(CorrelationId correlationId, String errorMessage) {
        Event event = new TokensNotIssuedEvent(correlationId, errorMessage).asEvent();
        messageQueue.publish(event);
    }

    @Override
    public void createTokenValidatedEvent(CorrelationId correlationId, Id customerId) {
        Event event = new TokenValidatedEvent(correlationId, customerId).asEvent();
        messageQueue.publish(event);
    }

    @Override
    public void createTokenNotValidatedEvent(CorrelationId correlationId, String errorMessage) {
        Event event = new TokenNotValidatedEvent(correlationId, errorMessage).asEvent();
        messageQueue.publish(event);
    }

}
