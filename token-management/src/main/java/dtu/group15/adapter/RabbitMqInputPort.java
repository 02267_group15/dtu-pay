/** @author Jens */
package dtu.group15.adapter;

import dtu.group15.events.inputs.*;
import dtu.group15.model.*;
import dtu.group15.service.TokenService;
import messaging.MessageQueue;

public class RabbitMqInputPort {

    private final TokenService tokenService;

    //Constructor that adds all the handlers for RabbitMQ
    public RabbitMqInputPort(TokenService service, MessageQueue messageQueue) {
        this.tokenService = service;

        messageQueue.addHandler(CustomerExistedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerExistedEvent.class)));
        messageQueue.addHandler(IssueTokensRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, IssueTokensRequestedEvent.class)));
        messageQueue.addHandler(PaymentRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, PaymentRequestedEvent.class)));
        messageQueue.addHandler(CustomerDeregistrationRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerDeregistrationRequestedEvent.class)));
    }

    /* Handlers for incoming events */

    private void apply(CustomerExistedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        tokenService.updateCustomerExists(correlationId, customerId);
    }

    private void apply(IssueTokensRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        Amount amount = e.getAmount();
        tokenService.issueTokens(correlationId, customerId, amount);
    }

    private void apply(PaymentRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        tokenService.validateToken(correlationId, e.getToken());
    }

    private void apply(CustomerDeregistrationRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        tokenService.cleanupToken(correlationId, customerId);
    }

}
