package dtu.group15.startup;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.adapter.RabbitMqInputPort;
import dtu.group15.adapter.RabbitMqOutputPort;
import dtu.group15.repository.TokenRepository;
import dtu.group15.service.TokenService;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messaging.implementations.RabbitMqQueue;

@QuarkusMain
public class Main {

    public static void main(String[] args) {
        Quarkus.run(StartUp.class, args);
    }

    public static class StartUp implements QuarkusApplication {
        @Override
        public int run(String... args) throws Exception {
            var mq = new RabbitMqQueue("rabbit-mq");
            var tokenRepository = new TokenRepository();
            IOutputPort outputPort = new RabbitMqOutputPort(mq);
            TokenService tokenService = new TokenService(tokenRepository, outputPort);

            new RabbitMqInputPort(tokenService, mq);

            Quarkus.waitForExit();
            return 0;
        }
    }
}
