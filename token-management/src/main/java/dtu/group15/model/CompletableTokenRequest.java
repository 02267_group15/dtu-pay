package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompletableTokenRequest {
    Id customerId;
    Amount amount;
    Boolean exists = null;

    public boolean isComplete() {
        return customerId != null && amount != null && exists != null;
    }
}
