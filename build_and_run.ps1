Set-PSDebug -Trace 1
$ErrorActionPreference = 'Stop'

try {
	# Stop and remove all docker containers and images
	#docker stop $(docker ps -a -q)
	#docker rm $(docker ps -a -q)
	#docker rmi -f $(docker images -aq)
	#docker image prune -f

	# Build and deploy of microservices
	pushd messaging-utilities-3.4
	mvn clean install
	popd

	pushd account-management
	mvn test
	mvn clean package
	popd

	pushd payment-management
	mvn test
	mvn clean package
	popd

	pushd token-management
	mvn test
	mvn clean package
	popd

	pushd facade
	mvn test
	mvn clean package
	popd

	docker image prune -f
	docker-compose down --remove-orphans
	docker-compose build
	docker-compose up -d

	# Give the Web server a chance to finish start up
	sleep 2

	# Run end-to-end tests
	pushd test-client
	mvn clean
	mvn test
	popd
} catch {
	$_
} finally {
	Read-Host -Prompt "Press Enter to exit"	
}
