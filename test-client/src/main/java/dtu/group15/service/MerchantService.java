/** @author Christian */
package dtu.group15.service;

import dtu.group15.model.*;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

public class MerchantService {
    WebTarget r;
    Client client;

    public MerchantService() {
        client = ClientBuilder.newClient();
        r = client.target("http://localhost:8080/").path("merchants");
    }

    //Send register merchant request.
    public Merchant register(String merchantBankId) {
        Merchant merchant = new Merchant();
        BankId bankId = new BankId(merchantBankId);
        merchant.setBankId(bankId);
        return r.request().post(Entity.entity(merchant, MediaType.APPLICATION_JSON_TYPE), Merchant.class);
    }

    //Send deregister merchant request.
    public void deregister(String id) {
        r.path(id).request().delete(String.class);
    }

    //Send get merchant report request.
    public MerchantReport getReport(String id) {
        return r.path(id).path("payments").request().get(MerchantReport.class);
    }

    //Close client.
    public void close() {
        client.close();
    }
}
