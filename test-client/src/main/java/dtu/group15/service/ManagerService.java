/** @author Magnus */
package dtu.group15.service;

import dtu.group15.model.Id;
import dtu.group15.model.ManagerReport;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;

import java.util.List;

public class ManagerService {
    WebTarget r;
    Client client;

    public ManagerService() {
        client = ClientBuilder.newClient();
        r = client.target("http://localhost:8080/").path("payments");
    }

    //Send a get manager report request.
    public ManagerReport getReport() {
        return r.request().get(ManagerReport.class);
    }

    //Close client.
    public void close() {
        client.close();
    }
}
