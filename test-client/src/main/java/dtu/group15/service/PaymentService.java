/** @author Aleksander */
package dtu.group15.service;

import dtu.group15.model.Amount;
import dtu.group15.model.Id;
import dtu.group15.model.MerchantPayment;
import dtu.group15.model.Token;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

public class PaymentService {
    WebTarget r;
    Client client;

    public PaymentService() {
        client = ClientBuilder.newClient();
        r = client.target("http://localhost:8080/").path("payments");
    }

    //Send payment request.
    public MerchantPayment pay(String mid, String token, int amount) {
        MerchantPayment paymentRequest = new MerchantPayment();
        Id id = new Id(mid);
        Token token1 = new Token(token);
        Amount amount1 = new Amount(amount);

        paymentRequest.setMerchantId(id);
        paymentRequest.setToken(token1);
        paymentRequest.setAmount(amount1);

        return r.request().post(Entity.entity(paymentRequest, MediaType.APPLICATION_JSON), MerchantPayment.class);
    }

    //Close client.
    public void close() {
        client.close();
    }
}
