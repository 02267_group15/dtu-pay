/** @author Nicolai */
package dtu.group15.service;

import dtu.group15.model.*;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;

import java.util.List;

public class CustomerService {
    WebTarget r;
    Client client;

    public CustomerService() {
        client = ClientBuilder.newClient();
        r = client.target("http://localhost:8080/").path("customers");
    }

    //Send register customer request.
    public Customer register(String customerBankId) {
        Customer customer = new Customer();
        BankId bankId = new BankId(customerBankId);
        customer.setBankId(bankId);
        return r.request().post(Entity.entity(customer, MediaType.APPLICATION_JSON_TYPE), Customer.class);
    }

    //Send a deregister customer request.
    public void deregister(String id) {
        r.path(id).request().delete(String.class);
    }

    //Send an issue tokens request.
    public List<Token> issueTokens(String id, int amount) {
        TokenRequest tokenRequest = new TokenRequest(new Id(id), new Amount(amount));
        Entity entity = Entity.entity(tokenRequest, MediaType.APPLICATION_JSON);
        TokenList tokenList = r.path(id).path("tokens").request().post(entity, TokenList.class);
        return tokenList.getTokenList();
    }

    //Send a get customer report request.
    public CustomerReport getReport(String id) {
        return r.path(id).path("payments").request().get(CustomerReport.class);
    }

    //Close the client.
    public void close() {
        client.close();
    }
}
