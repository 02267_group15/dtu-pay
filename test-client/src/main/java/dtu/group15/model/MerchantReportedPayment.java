package dtu.group15.model;

import lombok.Data;

@Data
public class MerchantReportedPayment {
    //Payment returned to merchant

    Token token;
    Amount amount;
}