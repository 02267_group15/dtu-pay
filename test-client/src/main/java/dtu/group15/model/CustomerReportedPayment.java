package dtu.group15.model;

import lombok.Data;

@Data
public class CustomerReportedPayment {
    //Payment returned to customer

    Id merchantId;
    Token token;
    Amount amount;
}