package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Amount implements Serializable {
    private static final long serialVersionUID = 1L;
    int amount;
}