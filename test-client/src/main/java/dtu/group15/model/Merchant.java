package dtu.group15.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Merchant {
    private Id id;
    private BankId bankId;
}
