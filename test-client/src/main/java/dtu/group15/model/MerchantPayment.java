package dtu.group15.model;

import lombok.Data;

@Data
public class MerchantPayment {
    Id merchantId;
    Token token;
    Amount amount;
}
