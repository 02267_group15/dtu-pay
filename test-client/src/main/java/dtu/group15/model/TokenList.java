package dtu.group15.model;

import lombok.Data;

import java.util.List;

@Data
public class TokenList {
    List<Token> tokenList;
}
