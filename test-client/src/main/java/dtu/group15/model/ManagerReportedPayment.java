package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManagerReportedPayment implements Serializable {
    //Payment returned to manager
    private static final long serialVersionUID = 1L;
    private Id paymentId;
    private Id customerId;
    private Id merchantId;
    private Amount amount;
    private Token token;
}
