/** @author Magnus */
package dtu.group15;

import io.cucumber.java.Before;
import io.cucumber.java.en.Then;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SharedSteps {

    @Before
    public void resetSharedData() {
        SharedData.reset();
    }

    @Then("an error message is given saying {string}")
    public void anErrorMessageIsGivenSaying(String msg) {
        assertEquals(msg, SharedData.exception.getResponse().readEntity(String.class));
    }

    @Then("no error message is given")
    public void noErrorMessageIsGiven() {
        assertNull(SharedData.exception);
    }
}
