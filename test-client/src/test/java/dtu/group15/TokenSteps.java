/** @author Nicolai */
package dtu.group15;

import dtu.group15.model.Token;
import dtu.group15.service.CustomerService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.WebApplicationException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TokenSteps {
    private final CustomerService customerService = new CustomerService();

    private List<Token> receivedTokens = new ArrayList<>();

    @Given("the customer has generated one token in DTU Pay")
    public void theCustomerHasGeneratedOneTokenInDTUPay() {
        try {
            SharedData.token = customerService.issueTokens(SharedData.customer.getId().getId(), 1).get(0);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @Given("the merchant uses an incorrect token")
    public void theMerchantUsesAnIncorrectToken() {
        SharedData.token = new Token();
        SharedData.token.setToken(UUID.randomUUID().toString());
    }

    @When("the customer issues {int} new token\\(s)")
    public void theCustomerIssuesNewTokenS(int amount) {
        try {
            receivedTokens = customerService.issueTokens(SharedData.customer.getId().getId(), amount);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @Then("the customer receives {int} token\\(s)")
    public void theCustomerReceivesTokenS(int amount) {
        assertEquals(amount, receivedTokens.size());
    }
}
