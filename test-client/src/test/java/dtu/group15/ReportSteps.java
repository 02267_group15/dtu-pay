/** @author Jens */
package dtu.group15;

import dtu.group15.model.*;
import dtu.group15.service.CustomerService;
import dtu.group15.service.ManagerService;
import dtu.group15.service.MerchantService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.WebApplicationException;
import org.junit.After;

import static io.smallrye.common.constraint.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

public class ReportSteps {

    private final ManagerService managerService = new ManagerService();
    private final CustomerService customerService = new CustomerService();
    private final MerchantService merchantService = new MerchantService();

    private ManagerReport initialReport;
    private ManagerReport managerReport;
    private CustomerReport customerReport;
    private MerchantReport merchantReport;

    @Given("an initial report request is made by the manager")
    public void anInitialReportRequestIsMadeByTheManager() {
        initialReport = managerService.getReport();
    }

    @When("a report request is made by the manager")
    public void aReportRequestIsMadeByTheManager() {
        managerReport = managerService.getReport();
    }

    @When("the customer requests the report of previous payments")
    public void theCustomerRequestsTheReportOfPreviousPayments() {
        try {
            customerReport = customerService.getReport(SharedData.customer.getId().getId());
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @When("the merchant requests the report of previous payments")
    public void theMerchantRequestsTheReportOfPreviousPayments() {
        try {
            merchantReport = merchantService.getReport(SharedData.merchant.getId().getId());
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @Then("a list of customer reported payments which contains a payment of {int} kr with the consumed token and the ID of the merchant")
    public void aListOfCustomerReportedPaymentsWhichContainsAPaymentOfKrWithTheConsumedTokenAndTheIDOfTheMerchant(int amount) {
        CustomerReportedPayment payment = customerReport.getCustomerReportedPayments().getFirst();
        assertEquals(amount, payment.getAmount().getAmount());
        assertEquals(SharedData.token, payment.getToken());
        assertEquals(SharedData.merchant.getId().getId(), payment.getMerchantId().getId());
    }

    @Then("a list of merchant reported payments which contains a payment of {int} kr with the consumed token")
    public void aListOfMerchantReportedPaymentsWhichContainsAPaymentOfKrWithTheConsumedToken(int amount) {
        MerchantReportedPayment payment = merchantReport.getMerchantReportedPayments().getFirst();
        assertEquals(amount, payment.getAmount().getAmount());
        assertEquals(SharedData.token, payment.getToken());
    }

    @Then("the returned list to the manager contains a payment from the customer to the merchant of {int} kr")
    public void theReturnedListToTheManagerContainsAPaymentOfKr(int amount) {
        boolean success = false;
        for (ManagerReportedPayment payment : managerReport.getPayments()) {
            if (payment.getCustomerId().equals(SharedData.customer.getId()) && payment.getMerchantId().equals(SharedData.merchant.getId()) && amount == payment.getAmount().getAmount()) {
                success = true;
                break;
            }
        }
        assertTrue(success);
    }

    @Then("the returned list to the manager has initial amount plus {int} element\\(s)")
    public void theReturnedListToTheManagerHasInitialAmountPlusElementS(int elements) {
        assertEquals(initialReport.getPayments().size() + elements, managerReport.getPayments().size());
    }

    @Then("the total amount of money transferred is the initial amount plus {int} kr")
    public void theTotalAmountOfMoneyTransferredIsTheInitialAmountPlusKr(int amount) {
        assertEquals(initialReport.getTotalAmount().getAmount() + amount, managerReport.getTotalAmount().getAmount());
    }

    @Then("the customer payment report is empty")
    public void theCustomerPaymentReportIsEmpty() {
        try {
            assertTrue(customerService.getReport(SharedData.customer.getId().getId()).getCustomerReportedPayments().isEmpty());
        } catch (WebApplicationException e) {
            assertEquals("", e.getResponse().readEntity(String.class));
        }
    }

    @Then("the merchant payment report is empty")
    public void theMerchantPaymentReportIsEmpty() {
        assertTrue(merchantService.getReport(SharedData.merchant.getId().getId()).getMerchantReportedPayments().isEmpty());
    }

    @After
    public void closeClients() {
        managerService.close();
    }
}
