/** @author Magnus */
package dtu.group15;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/* Important:
For Cucumber tests to be recognized by Maven, the class name has to have
either the word Test or Tests in the beginning or at the end.
For example, the class name CucumberTestExample will be ignored by Maven.
*/

@RunWith(Cucumber.class)
@CucumberOptions(plugin="summary"
        , publish= false
        , features = "src/test/resources/features"  // directory of the feature files
        , snippets = CucumberOptions.SnippetType.CAMELCASE
)
public class CucumberTest {
}