/** @author Christian */
package dtu.group15;

import dtu.group15.model.Customer;
import dtu.group15.model.Merchant;
import dtu.group15.model.Token;
import dtu.ws.fastmoney.User;
import jakarta.ws.rs.WebApplicationException;

public class SharedData {
    public static int userNum = 1;
    public static  Customer customer;
    public static Merchant merchant;
    public static Token token;
    public static WebApplicationException exception;

    public static void reset() {
        customer = null;
        merchant = null;
        token = null;
        exception = null;
        userNum = 1;
    }

    public static User createUser(String firstName, String lastName, String cpr) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCprNumber(cpr);
        return user;
    }

    public static User generateUser() {
        User user = createUser("First name " + userNum, "Last name " + userNum, "G15-"+userNum);
        userNum++;
        return user;
    }
}
