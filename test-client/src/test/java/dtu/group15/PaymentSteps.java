/** @author Michael */
package dtu.group15;

import dtu.group15.model.*;
import dtu.group15.service.*;
import dtu.ws.fastmoney.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.WebApplicationException;
import org.junit.After;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class PaymentSteps {
    private final PaymentService paymentService = new PaymentService();

    @When("a payment of {int} kr to the merchant is made using the token")
    public void aPaymentOfKrToTheMerchantIsMadeUsingTheToken(int amount) {
        try {
            paymentService.pay(SharedData.merchant.getId().getId(), SharedData.token.getToken(), amount);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @After
    public void closeClients() {
        paymentService.close();
    }
}
