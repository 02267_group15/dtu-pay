/** @author Aleksander */
package dtu.group15;

import dtu.group15.model.*;
import dtu.group15.service.CustomerService;
import dtu.group15.service.MerchantService;
import dtu.ws.fastmoney.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import jakarta.ws.rs.WebApplicationException;
import org.junit.After;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AccountsSteps {
    private final CustomerService customerService = new CustomerService();
    private final MerchantService merchantService = new MerchantService();
    private final BankService bank = new BankServiceService().getBankServicePort();

    private final List<Merchant> addedMerchants = new ArrayList<>();
    private final List<Customer> addedCustomers = new ArrayList<>();

    @Given("a customer with no ID has a bank account with balance {int}")
    public void aCustomerWithNoIDHasABankAccountWithBalance(int balance) throws BankServiceException_Exception {
        User user = SharedData.generateUser();
        String accountID = bank.createAccountWithBalance(user, new BigDecimal(balance));
        SharedData.customer = new Customer();
        BankId bankId = new BankId(accountID);
        SharedData.customer.setBankId(bankId);
    }

    @Given("the customer is registered in DTU Pay")
    public void theCustomerIsRegisteredInDTUPay() {
        SharedData.customer = customerService.register(SharedData.customer.getBankId().getId());
        addedCustomers.add(SharedData.customer);
    }

    @Given("a customer is registered in DTU Pay")
    public void aCustomerIsRegisteredInDTUPay() {
        SharedData.customer = customerService.register("Test bank ID");
        addedCustomers.add(SharedData.customer);
    }

    @Given("a customer which is not registered in DTU Pay")
    public void aCustomerWhichIsNotRegisteredInDTUPay() {
        SharedData.customer = new Customer();
        SharedData.customer.setId(new Id("NotRegistered"));
        addedCustomers.add(SharedData.customer);
    }

    @Given("a customer with no ID and bank account ID {string}")
    public void aCustomerWithNoIDAndBankAccountID(String bankAccountID) {
        SharedData.customer = new Customer();
        SharedData.customer.setBankId(new BankId(bankAccountID));
    }

    @When("the customer is deregistered in DTU Pay")
    public void theCustomerIsDeregisteredInDTUPay() {
        try {
            customerService.deregister(SharedData.customer.getId().getId());
            addedCustomers.remove(SharedData.customer);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @When("the given customer is registered in DTU Pay")
    public void theGivenCustomerIsRegisteredInDTUPay() {
        try {
            SharedData.customer = customerService.register(SharedData.customer.getBankId().getId());
            addedCustomers.add(SharedData.customer);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @Then("the customer has a bank balance of {int} kr")
    public void theCustomerHasABankBalanceOfKr(int balance) throws BankServiceException_Exception {
        assertEquals(new BigDecimal(balance), bank.getAccount(SharedData.customer.getBankId().getId()).getBalance());
    }

    @Then("a customer account with an ID and the bank ID {string} is received")
    public void aCustomerAccountWithAnIDAndTheProvidedBankIDIsReceived(String bankAccountID) {
        assertNotNull(SharedData.customer.getId());
        assertEquals(bankAccountID, SharedData.customer.getBankId().getId());
    }

    @Given("a merchant with no ID has a bank account with balance {int}")
    public void aMerchantWithNoIDHasABankAccountWithBalance(int balance) throws BankServiceException_Exception {
        User user = SharedData.generateUser();
        String accountID = bank.createAccountWithBalance(user, new BigDecimal(balance));
        SharedData.merchant = new Merchant();
        BankId bankId = new BankId(accountID);
        SharedData.merchant.setBankId(bankId);
    }

    @Given("the merchant is registered in DTU Pay")
    public void theMerchantIsRegisteredInDTUPay() {
        SharedData.merchant = merchantService.register(SharedData.merchant.getBankId().getId());
        addedMerchants.add(SharedData.merchant);
    }

    @Given("a merchant is registered in DTU Pay")
    public void aMerchantIsRegisteredInDTUPay() {
        SharedData.merchant = merchantService.register("Test bank ID");
        addedMerchants.add(SharedData.merchant);
    }

    @Given("a merchant which is not registered in DTU Pay")
    public void aMerchantWhichIsNotRegisteredInDTUPay() {
        SharedData.merchant = new Merchant();
        SharedData.merchant.setId(new Id("NotRegistered"));
        addedMerchants.add(SharedData.merchant);
    }

    @Given("a merchant with no ID and bank account ID {string}")
    public void aMerchantWithNoIDAndBankAccountID(String bankAccountID) {
        SharedData.merchant = new Merchant();
        SharedData.merchant.setBankId(new BankId(bankAccountID));
    }

    @Given("the merchant is not registered in DTU Pay")
    public void theMerchantIsNotRegisteredInDTUPay() {
        SharedData.merchant.setId(new Id("NotRegistered"));
    }

    @When("the merchant is deregistered in DTU Pay")
    public void theMerchantIsDeregisteredInDTUPay() {
        try {
            merchantService.deregister(SharedData.merchant.getId().getId());
            addedMerchants.remove(SharedData.merchant);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @When("the given merchant is registered in DTU Pay")
    public void theGivenMerchantIsRegisteredInDTUPay() {
        try {
            SharedData.merchant = merchantService.register(SharedData.merchant.getBankId().getId());
            addedMerchants.add(SharedData.merchant);
        } catch (WebApplicationException e) {
            SharedData.exception = e;
        }
    }

    @Then("the merchant has a bank balance of {int} kr")
    public void theMerchantHasABankBalanceOfKr(int balance) throws BankServiceException_Exception {
        assertEquals(new BigDecimal(balance), bank.getAccount(SharedData.merchant.getBankId().getId()).getBalance());
    }

    @Then("a merchant account with an ID and the bank ID {string} is received")
    public void aMerchantAccountWithAnIDAndTheProvidedBankIDIsReceived(String bankAccountID) {
        assertNotNull(SharedData.merchant.getId());
        assertEquals(bankAccountID, SharedData.merchant.getBankId().getId());
    }

    @Before
    @After
    public void deleteBankAccounts() {
        int i = 1;
        while (true) {
            try {
                Account account = bank.getAccountByCprNumber("G15-"+i);
                bank.retireAccount(account.getId());
            } catch (Exception e) {
                break;
            }
            i++;
        }
    }

    @After
    public void deleteDTUPayAccounts() {
        for (Customer customer : addedCustomers) {
            try {
                customerService.deregister(customer.getId().getId());
            } catch (Exception ignored) {}
        }

        for (Merchant merchant : addedMerchants) {
            try {
                merchantService.deregister(merchant.getId().getId());
            } catch (Exception ignored) {}
        }
    }

    @After
    public void closeClients() {
        customerService.close();
        merchantService.close();
    }
}
