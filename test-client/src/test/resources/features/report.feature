Feature: Report

  # @author Christian
  Scenario: Report for failed payment due to incorrect token
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the merchant uses an incorrect token
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Token is not valid"
    And the customer payment report is empty
    And the merchant payment report is empty

  # @author Christian
  Scenario: Report for failed payment due to low bank balance
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 10000 kr to the merchant is made using the token
    Then an error message is given saying "Debtor balance will be negative"
    And the customer payment report is empty
    And the merchant payment report is empty

  # @author Christian
  Scenario: Report for failed payment due to merchant not being registered in DTU Pay
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is not registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 10000 kr to the merchant is made using the token
    Then an error message is given saying "The merchant is not registered in DTU Pay"
    And the customer payment report is empty

  # @author Christian
  Scenario: Report for failed payment due to customer being deregistered in DTU Pay before the payment request
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    And the customer is deregistered in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Token is not valid"
    And the merchant payment report is empty

  # @author Nicolai
  Scenario: Merchant requests reports of previous payments successfully
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    And a payment of 100 kr to the merchant is made using the token
    When the merchant requests the report of previous payments
    Then no error message is given
    And a list of merchant reported payments which contains a payment of 100 kr with the consumed token

  # @author Nicolai
  Scenario: Failed merchant request of reports of previous payments due to merchant not existing in DTU Pay
    Given a merchant which is not registered in DTU Pay
    When the merchant requests the report of previous payments
    Then an error message is given saying "The merchant is not registered in DTU Pay"

  # @author Nicolai
  Scenario: Customer requests reports of previous payments successfully
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    And a payment of 100 kr to the merchant is made using the token
    When the customer requests the report of previous payments
    Then no error message is given
    And a list of customer reported payments which contains a payment of 100 kr with the consumed token and the ID of the merchant

  # @author Nicolai
  Scenario: Failed customer request of reports of previous payments due to customer not existing in DTU Pay
    Given a customer which is not registered in DTU Pay
    When the customer requests the report of previous payments
    Then an error message is given saying "The customer is not registered in DTU Pay"

  # @author Nicolai
  Scenario: Manager requests report when no payments have been made successfully
    Given an initial report request is made by the manager
    When a report request is made by the manager
    Then no error message is given
    And the returned list to the manager has initial amount plus 0 element(s)
    And the total amount of money transferred is the initial amount plus 0 kr

  # @author Nicolai
  Scenario: Manager requests report with a payment successfully
    Given an initial report request is made by the manager
    And a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    And a payment of 100 kr to the merchant is made using the token
    When a report request is made by the manager
    Then no error message is given
    And the returned list to the manager has initial amount plus 1 element(s)
    And the returned list to the manager contains a payment from the customer to the merchant of 100 kr
    And the total amount of money transferred is the initial amount plus 100 kr
