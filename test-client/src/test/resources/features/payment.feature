Feature: Payment
  # @author Michael
  Scenario: Successful payment
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then no error message is given
    And the customer has a bank balance of 900 kr
    And the merchant has a bank balance of 1100 kr

  # @author Michael
  Scenario: Failed payment due to reuse of token
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    And a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Token is not valid"
    And the customer has a bank balance of 900 kr
    And the merchant has a bank balance of 1100 kr

  # @author Michael
  Scenario: Failed payment due to incorrect token
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the merchant uses an incorrect token
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Token is not valid"
    And the customer has a bank balance of 1000 kr
    And the merchant has a bank balance of 1000 kr

  # @author Michael
  Scenario: Failed payment due to low bank balance
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 10000 kr to the merchant is made using the token
    Then an error message is given saying "Debtor balance will be negative"
    And the customer has a bank balance of 1000 kr
    And the merchant has a bank balance of 1000 kr

  # @author Michael
  Scenario: Failed payment due to merchant not being registered in DTU Pay
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is not registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 10000 kr to the merchant is made using the token
    Then an error message is given saying "The merchant is not registered in DTU Pay"
    And the customer has a bank balance of 1000 kr
    And the merchant has a bank balance of 1000 kr

  # @author Aleksander
  Scenario: Failed payment due to merchant being deregistered in DTU Pay before the payment request
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the merchant is deregistered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "The merchant is not registered in DTU Pay"
    And the customer has a bank balance of 1000 kr
    And the merchant has a bank balance of 1000 kr

  # @author Aleksander
  Scenario: Failed payment due to customer being deregistered in DTU Pay before the payment request
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    And the customer is deregistered in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Token is not valid"
    And the customer has a bank balance of 1000 kr
    And the merchant has a bank balance of 1000 kr

  # @author Aleksander
  Scenario: Failed payment due to customer not having a bank account
    Given a customer is registered in DTU Pay
    And a merchant with no ID has a bank account with balance 1000
    And the merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Debtor account does not exist"

  # @author Aleksander
  Scenario: Failed payment due to merchant not having a bank account
    Given a customer with no ID has a bank account with balance 1000
    And the customer is registered in DTU Pay
    And a merchant is registered in DTU Pay
    And the customer has generated one token in DTU Pay
    When a payment of 100 kr to the merchant is made using the token
    Then an error message is given saying "Creditor account does not exist"


