# @author Magnus
Feature: Token
  Scenario: Issue tokens successfully
  Given a customer is registered in DTU Pay
  When the customer issues 1 new token(s)
  Then no error message is given
  And the customer receives 1 token(s)

  Scenario: Failed to issue token(s) when customer has too many tokens
  Given a customer is registered in DTU Pay
  And the customer issues 3 new token(s)
  And the customer receives 3 token(s)
  When the customer issues 3 new token(s)
  Then an error message is given saying "Customer cannot request any more tokens"

  Scenario: Failed to issue token(s) when customer requests too many tokens
  Given a customer is registered in DTU Pay
  And the customer issues 10 new token(s)
  Then an error message is given saying "Amount of tokens requested must be between 1 and 5"

  Scenario: Failed to issue token(s) when customer does not exist in DTU Pay
  Given a customer which is not registered in DTU Pay
  And the customer issues 1 new token(s)
  Then an error message is given saying "The customer is not registered in DTU Pay"