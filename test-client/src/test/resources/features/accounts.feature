# @author Jens
Feature: Accounts
  Scenario: Register customer successfully
    Given a customer with no ID and bank account ID "Test bank ID"
    When the given customer is registered in DTU Pay
    Then no error message is given
    And a customer account with an ID and the bank ID "Test bank ID" is received

  Scenario: Deregister customer successfully in DTU Pay
    Given a customer is registered in DTU Pay
    When the customer is deregistered in DTU Pay
    Then no error message is given

  Scenario: Failed deregister of customer that does not exist in DTU Pay
    Given a customer which is not registered in DTU Pay
    When the customer is deregistered in DTU Pay
    Then an error message is given saying "The customer is not registered in DTU Pay"

  Scenario: Register merchant successfully
    Given a merchant with no ID and bank account ID "Test bank ID"
    When the given merchant is registered in DTU Pay
    Then no error message is given
    And a merchant account with an ID and the bank ID "Test bank ID" is received

  Scenario: Deregister merchant successfully in DTU Pay
    Given a merchant is registered in DTU Pay
    When the merchant is deregistered in DTU Pay
    Then no error message is given

  Scenario: Failed deregister of merchant that does not exist in DTU Pay
    Given a merchant which is not registered in DTU Pay
    When the merchant is deregistered in DTU Pay
    Then an error message is given saying "The merchant is not registered in DTU Pay"


