/** @author Jens */
package dtu.group15;

import dtu.group15.events.inputs.*;
import dtu.group15.events.outputs.*;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.function.Consumer;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AccountManagerTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void paymentRequestedSuccessfullyTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant with bank id
        BankId merchantBankID = new BankId("Merchant Bank ID");

        //register merchant
        Event merchantRegistrationRequestedEvent = new MerchantRegistrationRequestedEvent(correlationId, merchantBankID).asEvent();
        TestUtil.publishEvent(consumerMap, merchantRegistrationRequestedEvent, MerchantRegistrationRequestedEvent.class);

        //merchant registered
        Event capturedMerchantRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedMerchantRegisteredEvent);
        MerchantRegisteredEvent merchantRegisteredEvent = capturedMerchantRegisteredEvent.getArgument(0, MerchantRegisteredEvent.class);
        Id merchantId = merchantRegisteredEvent.getId();
        Event expected = new MerchantRegisteredEvent(correlationId, merchantId, merchantBankID).asEvent();
        assertEquals(expected, capturedMerchantRegisteredEvent);

        //init customer with bank id
        BankId customerBankID = new BankId("Customer Bank ID");

        //register customer
        Event customerRegistrationRequestedEvent = new CustomerRegistrationRequestedEvent(correlationId, customerBankID).asEvent();
        TestUtil.publishEvent(consumerMap, customerRegistrationRequestedEvent, CustomerRegistrationRequestedEvent.class);

        //customer registered
        Event capturedCustomerRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedCustomerRegisteredEvent);
        CustomerRegisteredEvent customerRegisteredEvent = capturedCustomerRegisteredEvent.getArgument(0, CustomerRegisteredEvent.class);
        Id customerId = customerRegisteredEvent.getId();
        expected = new CustomerRegisteredEvent(correlationId, customerId, customerBankID).asEvent();
        assertEquals(expected, capturedCustomerRegisteredEvent);

        //payment requested
        Token token = new Token("Valid token");
        Amount amount = new Amount(100);
        Event paymentRequestedEvent = new PaymentRequestedEvent(correlationId, merchantId, token, amount).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        //merchant bank id acquired
        Event capturedMerchantBankIDAcquiredEvent = capturedEvents.removeFirst();
        expected = new MerchantBankIDAcquiredEvent(correlationId, merchantId, merchantBankID).asEvent();
        assertEquals(expected, capturedMerchantBankIDAcquiredEvent);

        //token validated
        Event tokenValidatedEvent = new TokenValidatedEvent(correlationId, customerId).asEvent();
        TestUtil.publishEvent(consumerMap, tokenValidatedEvent, TokenValidatedEvent.class);

        //customer bank id acquired
        Event capturedCustomerBankIDAcquiredEvent = capturedEvents.removeFirst();
        expected = new CustomerBankIDAcquiredEvent(correlationId, customerId, customerBankID).asEvent();
        assertEquals(expected, capturedCustomerBankIDAcquiredEvent);

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void paymentRequestFailedTest() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant not registered in DTU Pay
        Merchant merchant = new Merchant();

        //payment requested
        Token token = new Token("Valid token");
        Amount amount = new Amount(100);
        Event paymentRequestedEvent = new PaymentRequestedEvent(correlationId, merchant.getId(), token, amount).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        //merchant bank id not acquired
        Event capturedMerchantBankIDNotAcquiredEvent = capturedEvents.removeFirst();
        Event expected = new MerchantBankIDNotAcquiredEvent(correlationId, "The merchant is not registered in DTU Pay").asEvent();
        assertEquals(expected, capturedMerchantBankIDNotAcquiredEvent);

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void issueTokensRequestedSuccessfulTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer
        BankId bankId = new BankId("Bank ID");

        //register customer
        Event customerRegistrationRequestedEvent = new CustomerRegistrationRequestedEvent(correlationId, bankId).asEvent();
        TestUtil.publishEvent(consumerMap, customerRegistrationRequestedEvent, CustomerRegistrationRequestedEvent.class);

        //customer registered
        Event capturedCustomerRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedCustomerRegisteredEvent);
        CustomerRegisteredEvent customerRegisteredEvent = capturedCustomerRegisteredEvent.getArgument(0, CustomerRegisteredEvent.class);
        Id id = customerRegisteredEvent.getId();
        Event expected = new CustomerRegisteredEvent(correlationId, id, bankId).asEvent();
        assertEquals(expected, capturedCustomerRegisteredEvent);

        //init token request
        Amount amount = new Amount(5);

        //issue tokens requested
        Event issueTokensRequestedEvent = new IssueTokensRequestedEvent(correlationId, id, amount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokensRequestedEvent, IssueTokensRequestedEvent.class);

        //customer existed
        Event customerExistsEvent = capturedEvents.removeFirst();
        expected = new CustomerExistedEvent(correlationId, id).asEvent();
        assertEquals(expected, customerExistsEvent);

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void issueTokensRequestedFailedTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer not registered in DTU Pay
        Id customerID = new Id("Random ID");

        //init token request
        Amount amount = new Amount(5);

        //issue tokens requested
        Event issueTokensRequestedEvent = new IssueTokensRequestedEvent(correlationId, customerID, amount).asEvent();
        TestUtil.publishEvent(consumerMap, issueTokensRequestedEvent, IssueTokensRequestedEvent.class);

        //customer existed
        Event customerDidNotExistEvent = capturedEvents.removeFirst();
        Event expected = new CustomerDidNotExistEvent(correlationId, customerID).asEvent();
        assertEquals(expected, customerDidNotExistEvent);

        assertTrue(capturedEvents.isEmpty());
    }

    @Test 
    public void customerDeregistrationRequestedSuccessfulTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer
        BankId bankId = new BankId("Bank ID");

        //register customer
        Event customerRegistrationRequestedEvent = new CustomerRegistrationRequestedEvent(correlationId, bankId).asEvent();
        TestUtil.publishEvent(consumerMap, customerRegistrationRequestedEvent, CustomerRegistrationRequestedEvent.class);

        //customer registered
        Event capturedCustomerRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedCustomerRegisteredEvent);
        CustomerRegisteredEvent customerRegisteredEvent = capturedCustomerRegisteredEvent.getArgument(0, CustomerRegisteredEvent.class);
        Id id = customerRegisteredEvent.getId();
        Event expected = new CustomerRegisteredEvent(correlationId, id, bankId).asEvent();
        assertEquals(expected, capturedCustomerRegisteredEvent);

        //customer deregistration request
        Event customerDeregistrationRequestedEvent = new CustomerDeregistrationRequestedEvent(correlationId, id).asEvent();
        TestUtil.publishEvent(consumerMap, customerDeregistrationRequestedEvent, CustomerDeregistrationRequestedEvent.class);

        //customer deregistered
        expected = new CustomerDeregisteredEvent(correlationId).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test 
    public void customerDeregistrationRequestedFailedTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer not registered with DTU Pay
        Id customerID = new Id("Random ID");
        BankId customerBankID = new BankId("Random Bank ID");
        Customer customer = new Customer(customerID, customerBankID);

        //customer deregistration request
        Event customerDeregistrationRequestedEvent = new CustomerDeregistrationRequestedEvent(correlationId, customer.getId()).asEvent();
        TestUtil.publishEvent(consumerMap, customerDeregistrationRequestedEvent, CustomerDeregistrationRequestedEvent.class);

        //customer deregistered
        Event expected = new CustomerNotDeregisteredEvent(correlationId, "The customer is not registered in DTU Pay").asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void merchantDeregistrationRequestedSuccessfulTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant
        BankId bankId = new BankId("Bank ID");

        //register merchant
        Event merchantRegistrationRequestedEvent = new MerchantRegistrationRequestedEvent(correlationId, bankId).asEvent();
        TestUtil.publishEvent(consumerMap, merchantRegistrationRequestedEvent, MerchantRegistrationRequestedEvent.class);

        //merchant registered
        Event capturedMerchantRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedMerchantRegisteredEvent);
        MerchantRegisteredEvent merchantRegisteredEvent = capturedMerchantRegisteredEvent.getArgument(0, MerchantRegisteredEvent.class);
        Id id = merchantRegisteredEvent.getId();
        Event expected = new MerchantRegisteredEvent(correlationId, id, bankId).asEvent();
        assertEquals(expected, capturedMerchantRegisteredEvent);

        //merchant deregistration request
        Event merchantDeregistrationRequestedEvent = new MerchantDeregistrationRequestedEvent(correlationId, id).asEvent();
        TestUtil.publishEvent(consumerMap, merchantDeregistrationRequestedEvent, MerchantDeregistrationRequestedEvent.class);

        //merchant deregistered
        expected = new MerchantDeregisteredEvent(correlationId).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void merchantDeregistrationRequestedFailedTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant not registered with DTU Pay
        Id merchantID = new Id("Random ID");
        BankId merchantBankID = new BankId("Random Bank ID");
        Merchant merchant = new Merchant(merchantID, merchantBankID);

        //merchant deregistration request
        Event merchantDeregistrationRequestedEvent = new MerchantDeregistrationRequestedEvent(correlationId, merchant.getId()).asEvent();
        TestUtil.publishEvent(consumerMap, merchantDeregistrationRequestedEvent, MerchantDeregistrationRequestedEvent.class);

        //customer deregistered
        Event expected = new MerchantNotDeregisteredEvent(correlationId, "The merchant is not registered in DTU Pay").asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }


    @Test
    public void customerReportRequestedSuccessTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer bank ID
        BankId customerBankID = new BankId("Random Bank ID");

        //register customer
        Event customerRegistrationRequestedEvent = new CustomerRegistrationRequestedEvent(correlationId, customerBankID).asEvent();
        TestUtil.publishEvent(consumerMap, customerRegistrationRequestedEvent, CustomerRegistrationRequestedEvent.class);

        //customer registered
        Event capturedCustomerRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedCustomerRegisteredEvent);
        CustomerRegisteredEvent customerRegisteredEvent = capturedCustomerRegisteredEvent.getArgument(0, CustomerRegisteredEvent.class);
        Id id = customerRegisteredEvent.getId();
        Event expected = new CustomerRegisteredEvent(correlationId, id, customerBankID).asEvent();
        assertEquals(expected, capturedCustomerRegisteredEvent);

        //customer report requested
        Event customerReportRequestedEvent = new CustomerReportRequestedEvent(correlationId, id).asEvent();
        TestUtil.publishEvent(consumerMap, customerReportRequestedEvent, CustomerReportRequestedEvent.class);

        //customer existed
        expected = new CustomerExistedEvent(correlationId, id).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void customerReportRequestedFailedTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init customer not registered in DTU Pay
        Id customerID = new Id("Random ID");
        BankId customerBankID = new BankId("Random Bank ID");
        Customer customer = new Customer(customerID, customerBankID);

        //customer report requested
        Event customerReportRequestedEvent = new CustomerReportRequestedEvent(correlationId, customer.getId()).asEvent();
        TestUtil.publishEvent(consumerMap, customerReportRequestedEvent, CustomerReportRequestedEvent.class);

        //customer existed
        Event expected = new CustomerDidNotExistEvent(correlationId, customer.getId()).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void merchantReportRequestedSuccessTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant bank ID
        BankId merchantBankID = new BankId("Random Bank ID");

        //register merchant
        Event merchantRegistrationRequestedEvent = new MerchantRegistrationRequestedEvent(correlationId, merchantBankID).asEvent();
        TestUtil.publishEvent(consumerMap, merchantRegistrationRequestedEvent, MerchantRegistrationRequestedEvent.class);

        //merchant registered
        Event capturedMerchantRegisteredEvent = capturedEvents.removeFirst();
        assertNotNull(capturedMerchantRegisteredEvent);
        MerchantRegisteredEvent merchantRegisteredEvent = capturedMerchantRegisteredEvent.getArgument(0, MerchantRegisteredEvent.class);
        Id id = merchantRegisteredEvent.getId();
        Event expected = new MerchantRegisteredEvent(correlationId, id, merchantBankID).asEvent();
        assertEquals(expected, capturedMerchantRegisteredEvent);

        //merchant report requested
        Event merchantReportRequestedEvent = new MerchantReportRequestedEvent(correlationId, id).asEvent();
        TestUtil.publishEvent(consumerMap, merchantReportRequestedEvent, MerchantReportRequestedEvent.class);

        //merchant existed
        expected = new MerchantExistedEvent(correlationId, id).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }

    @Test
    public void merchantReportRequestedFailedTest(){
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());

        //init merchant not registered in DTU Pay
        Id merchantID = new Id("Random ID");
        BankId merchantBankID = new BankId("Random Bank ID");
        Merchant merchant = new Merchant(merchantID, merchantBankID);

        //merchant report requested
        Event merchantReportRequestedEvent = new MerchantReportRequestedEvent(correlationId, merchant.getId()).asEvent();
        TestUtil.publishEvent(consumerMap, merchantReportRequestedEvent, MerchantReportRequestedEvent.class);

        //customer existed
        Event expected = new MerchantDidNotExistEvent(correlationId, merchant.getId()).asEvent();
        assertEquals(expected, capturedEvents.removeFirst());

        assertTrue(capturedEvents.isEmpty());
    }
}