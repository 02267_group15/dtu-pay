package dtu.group15.startup;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.adapter.RabbitMqAdapterInput;
import dtu.group15.adapter.RabbitMqAdapterOutput;
import dtu.group15.events.inputs.CustomerRegistrationRequestedEvent;
import dtu.group15.repository.CustomerRepository;
import dtu.group15.repository.MerchantRepository;
import dtu.group15.service.AccountService;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import messaging.implementations.RabbitMqQueue;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class Main {

    public static void main(String[] args) {
        Quarkus.run(StartUp.class, args);
    }

    public static class StartUp implements QuarkusApplication {
        @Override
        public int run(String... args) throws Exception {
            var mq = new RabbitMqQueue("rabbit-mq");
            var customerRepository = new CustomerRepository();
            var merchantRepository = new MerchantRepository();
            IOutputPort outputPort = new RabbitMqAdapterOutput(mq);
            AccountService accountService = new AccountService(customerRepository, merchantRepository, outputPort);

            new RabbitMqAdapterInput(accountService, mq);

            Quarkus.waitForExit();
            return 0;
        }
    }
}
