/** @author Magnus */
package dtu.group15.adapter;

import dtu.group15.model.*;

public interface IOutputPort {
    void createCustomerBankIdAcquiredEvent(CorrelationId correlationId, Id customerId, BankId bankId);

    void createCustomerDeregisteredEvent(CorrelationId correlationId);

    void createCustomerDidNotExistEvent(CorrelationId correlationId, Id customerId);

    void createCustomerExistedEvent(CorrelationId correlationId, Id customerId);

    void createCustomerNotDeregisteredEvent(CorrelationId correlationId, String errorMessage);

    void createCustomerRegisteredEvent(CorrelationId correlationId, Id customerId, BankId bankId);

    void createMerchantBankIdAcquiredEvent(CorrelationId correlationId, Id merchantId, BankId bankId);

    void createMerchantBankIdNotAcquiredEvent(CorrelationId correlationId, String errorMessage);

    void createMerchantDeregisteredEvent(CorrelationId correlationId);

    void createMerchantNotDeregisteredEvent(CorrelationId correlationId, String errorMessage);

    void createMerchantRegisteredEvent(CorrelationId correlationId, Id merchantId, BankId bankId);

    void createMerchantExistedEvent(CorrelationId correlationId, Id merchantId);

    void createMerchantDidNotExistEvent(CorrelationId correlationId, Id merchantId);

}
