/** @author Christian */
package dtu.group15.adapter;


import dtu.group15.events.inputs.*;
import dtu.group15.model.*;
import dtu.group15.service.AccountService;
import messaging.MessageQueue;

public class RabbitMqAdapterInput {
    private final AccountService accountService;

    //Constructor that adds all the handlers for RabbitMQ
    public RabbitMqAdapterInput(AccountService ac, MessageQueue messageQueue) {
        this.accountService = ac;

        messageQueue.addHandler(CustomerDeregistrationRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerDeregistrationRequestedEvent.class)));
        messageQueue.addHandler(CustomerRegistrationRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerRegistrationRequestedEvent.class)));
        messageQueue.addHandler(CustomerReportRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerReportRequestedEvent.class)));
        messageQueue.addHandler(IssueTokensRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, IssueTokensRequestedEvent.class)));
        messageQueue.addHandler(MerchantDeregistrationRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantDeregistrationRequestedEvent.class)));
        messageQueue.addHandler(MerchantRegistrationRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantRegistrationRequestedEvent.class)));
        messageQueue.addHandler(MerchantReportRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantReportRequestedEvent.class)));
        messageQueue.addHandler(PaymentRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, PaymentRequestedEvent.class)));
        messageQueue.addHandler(TokenValidatedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, TokenValidatedEvent.class)));

    }

    /* Handlers for incoming events */

    private void apply(CustomerDeregistrationRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        accountService.deregisterCustomer(correlationId, customerId);
    }

    private void apply(CustomerRegistrationRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        BankId bankId = e.getCustomerBankId();
        accountService.registerCustomer(correlationId, bankId);
    }

    private void apply(CustomerReportRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        accountService.checkCustomerExists(correlationId, customerId);

    }

    private void apply(IssueTokensRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        accountService.checkCustomerExists(correlationId, customerId);
    }

    private void apply(MerchantDeregistrationRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id merchantId = e.getMerchantId();
        accountService.deregisterMerchant(correlationId, merchantId);
    }

    private void apply(MerchantRegistrationRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        BankId bankId = e.getMerchantBankId();
        accountService.registerMerchant(correlationId, bankId);
    }

    private void apply(MerchantReportRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id merchantId = e.getMerchantId();
        accountService.checkMerchantExists(correlationId, merchantId);
    }

    private void apply(PaymentRequestedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        accountService.getMerchantBankId(correlationId, e.getMerchantId());
    }

    private void apply(TokenValidatedEvent e) {
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        accountService.getCustomerBankId(correlationId, customerId);
    }
}
