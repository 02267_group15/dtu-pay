/** @author Magnus */
package dtu.group15.adapter;

import dtu.group15.events.outputs.*;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;

public class RabbitMqAdapterOutput implements IOutputPort {
    private final MessageQueue messageQueue;
    public RabbitMqAdapterOutput(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    /* The methods used in this class are used to publish different events to the message queue */

    public void createCustomerBankIdAcquiredEvent(CorrelationId correlationId, Id customerId, BankId bankId) {
        Event event = new CustomerBankIDAcquiredEvent(correlationId, customerId, bankId).asEvent();
        messageQueue.publish(event);
    }

    public void createCustomerDeregisteredEvent(CorrelationId correlationId) {
        Event event = new CustomerDeregisteredEvent(correlationId).asEvent();
        messageQueue.publish(event);
    }

    public void createCustomerDidNotExistEvent(CorrelationId correlationId, Id customerId) {
        Event event = new CustomerDidNotExistEvent(correlationId, customerId).asEvent();
        messageQueue.publish(event);
    }

    public void createCustomerExistedEvent(CorrelationId correlationId, Id customerId) {
        Event event = new CustomerExistedEvent(correlationId, customerId).asEvent();
        messageQueue.publish(event);
    }

    public void createCustomerNotDeregisteredEvent(CorrelationId correlationId, String errorMessage) {
        Event event = new CustomerNotDeregisteredEvent(correlationId, errorMessage).asEvent();
        messageQueue.publish(event);
    }

    public void createCustomerRegisteredEvent(CorrelationId correlationId, Id customerId, BankId bankId) {
        Event event = new CustomerRegisteredEvent(correlationId, customerId, bankId).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantBankIdAcquiredEvent(CorrelationId correlationId, Id merchantId, BankId bankId) {
        Event event = new MerchantBankIDAcquiredEvent(correlationId, merchantId, bankId).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantBankIdNotAcquiredEvent(CorrelationId correlationId, String errorMessage) {
        Event event = new MerchantBankIDNotAcquiredEvent(correlationId, errorMessage).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantDeregisteredEvent(CorrelationId correlationId) {
        Event event = new MerchantDeregisteredEvent(correlationId).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantNotDeregisteredEvent(CorrelationId correlationId, String errorMessage) {
        Event event = new MerchantNotDeregisteredEvent(correlationId, errorMessage).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantRegisteredEvent(CorrelationId correlationId, Id merchantId, BankId bankId) {
        Event event = new MerchantRegisteredEvent(correlationId, merchantId, bankId).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantExistedEvent(CorrelationId correlationId, Id merchantId) {
        Event event = new MerchantExistedEvent(correlationId, merchantId).asEvent();
        messageQueue.publish(event);
    }

    public void createMerchantDidNotExistEvent(CorrelationId correlationId, Id merchantId) {
        Event event = new MerchantDidNotExistEvent(correlationId, merchantId).asEvent();
        messageQueue.publish(event);
    }


}
