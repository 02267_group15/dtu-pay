/** @author Aleksander */
package dtu.group15.service;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.model.*;
import dtu.group15.repository.CustomerRepository;
import dtu.group15.repository.MerchantRepository;

public class AccountService {
    CustomerRepository customerRepository;
    MerchantRepository merchantRepository;
    IOutputPort outputPort;

    public AccountService(CustomerRepository customerRepository, MerchantRepository merchantRepository, IOutputPort outputPort) {
        this.customerRepository = customerRepository;
        this.merchantRepository = merchantRepository;
        this.outputPort = outputPort;
    }

    //Register a customer and create send event
    public void registerCustomer(CorrelationId correlationId, BankId customerBankId) {
        Customer result = customerRepository.add(new Customer(null, customerBankId));
        outputPort.createCustomerRegisteredEvent(correlationId, result.getId(), result.getBankId());
    }

    //Register merchant and send event
    public void registerMerchant(CorrelationId correlationId, BankId merchantBankId) {
        Merchant result = merchantRepository.add(new Merchant(null, merchantBankId));
        outputPort.createMerchantRegisteredEvent(correlationId, result.getId(), result.getBankId());
    }

    //Deregister customer and send event
    public void deregisterCustomer(CorrelationId correlationId, Id customerId) {
        try {
            customerRepository.remove(customerId);
            outputPort.createCustomerDeregisteredEvent(correlationId);
        } catch (Exception e) {
            outputPort.createCustomerNotDeregisteredEvent(correlationId, e.getMessage());
        }
    }

    //Deregister merchant and send event
    public void deregisterMerchant(CorrelationId correlationId, Id merchantId) {
        try {
            merchantRepository.remove(merchantId);
            outputPort.createMerchantDeregisteredEvent(correlationId);
        } catch (Exception e) {
            outputPort.createMerchantNotDeregisteredEvent(correlationId, e.getMessage());
        }
    }

    //Checks if a customer exists and sends an event
    public void checkCustomerExists(CorrelationId correlationId, Id customerId) {
        if (customerRepository.containsId(customerId)) {
            outputPort.createCustomerExistedEvent(correlationId, customerId);
        }
        else {
            outputPort.createCustomerDidNotExistEvent(correlationId, customerId);
        }
    }

    //Checks if a merchant exists and sends an event
    public void checkMerchantExists(CorrelationId correlationId, Id merchantId) {
        if (merchantRepository.containsId(merchantId)) {
            outputPort.createMerchantExistedEvent(correlationId, merchantId);
        }
        else {
            outputPort.createMerchantDidNotExistEvent(correlationId, merchantId);
        }
    }

    //Gets the bank account ID of a customer and sends an event
    public void getCustomerBankId(CorrelationId correlationId, Id customerId) {
        BankId bankId = customerRepository.getBankId(customerId);
        outputPort.createCustomerBankIdAcquiredEvent(correlationId, customerId, bankId);
    }

    //Gets the bank account ID of a merchant and sends an event
    public void getMerchantBankId(CorrelationId correlationId, Id merchantId) {
        BankId bankId = merchantRepository.getBankId(merchantId);
        if (bankId == null) {
            outputPort.createMerchantBankIdNotAcquiredEvent(correlationId, "The merchant is not registered in DTU Pay");
        }
        else {
            outputPort.createMerchantBankIdAcquiredEvent(correlationId, merchantId, bankId);
        }
    }
}
