package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    Id customerId;
    Amount amount;
}
