package dtu.group15.events.inputs;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import dtu.group15.model.Amount;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Value;
import messaging.Event;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IssueTokensRequestedEvent extends DTUPayEvent {
    private static final long serialVersionUID = 1L;
    CorrelationId correlationId;
    Id customerId;
    Amount amount;
}
