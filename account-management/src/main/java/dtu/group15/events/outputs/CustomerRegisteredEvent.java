package dtu.group15.events.outputs;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import dtu.group15.model.BankId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomerRegisteredEvent extends DTUPayEvent {
    private static final long serialVersionUID = 1L;
    CorrelationId correlationId;
    private Id id;
    private BankId bankId;
}
