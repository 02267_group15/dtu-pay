/** @author Christian */
package dtu.group15.repository;

import dtu.group15.model.BankId;
import dtu.group15.model.Customer;
import dtu.group15.model.Id;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CustomerRepository {
    private final List<Customer> customers = new ArrayList<>();

    //Creates a random ID for a customer
    private Id createId() {
        return new Id(UUID.randomUUID().toString());
    }

    //Add a customer to the repository
    public Customer add(Customer customer) {
        customer.setId(createId());
        customers.add(customer);
        return customer;
    }

    //Remove a customer from the repository. Throws exception if the customer is not in the repository
    public void remove(Id id) throws IllegalArgumentException {
        boolean success = false;
        for (Customer customer : customers) {
            if (customer.getId().equals(id)) {
                customers.remove(customer);
                success = true;
                break;
            }
        }
        if (!success) {
            throw new IllegalArgumentException("The customer is not registered in DTU Pay");
        }
    }

    //Returns the bank account ID for a given customer ID. Returns null if the customer is not in the repository
    public BankId getBankId(Id id) {
        for (Customer customer : customers) {
            if (customer.getId().equals(id)) {
                return customer.getBankId();
            }
        }
        return null;
    }

    //Checks if the repository contains a specific customer ID
    public Boolean containsId(Id id) {
        return customers.stream().anyMatch(c -> c.getId().equals(id));
    }
}
