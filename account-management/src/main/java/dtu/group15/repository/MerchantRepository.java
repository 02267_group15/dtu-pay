/** @author Aleksander */
package dtu.group15.repository;

import dtu.group15.model.BankId;
import dtu.group15.model.Id;
import dtu.group15.model.Merchant;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MerchantRepository {
    private final List<Merchant> merchants = new ArrayList<>();

    //Create a random ID for a merchant
    private Id createId() {
        return new Id(UUID.randomUUID().toString());
    }

    //Add a merchant to the repository
    public Merchant add(Merchant merchant) {
        merchant.setId(createId());
        merchants.add(merchant);
        return merchant;
    }

    //Remove a merchant from the repository. Throws exception if the merchant is not in the repository
    public void remove(Id id) throws IllegalArgumentException {
        boolean success = false;
        for (Merchant merchant : merchants) {
            if (merchant.getId().equals(id)) {
                merchants.remove(merchant);
                success = true;
                break;
            }
        }
        if (!success) {
            throw new IllegalArgumentException("The merchant is not registered in DTU Pay");
        }
    }

    //Returns the bank account ID for a given merchant ID. Returns null if the merchant is not in the repository
    public BankId getBankId(Id id) {
        for (Merchant merchant : merchants) {
            if (merchant.getId().equals(id)) {
                return merchant.getBankId();
            }
        }
        return null;
    }

    //Checks if the repository contains a specific merchant ID
    public Boolean containsId(Id id) {
        return merchants.stream().anyMatch(m -> m.getId().equals(id));
    }
}
