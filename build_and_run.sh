#!/bin/bash
set -e

# Stop and remove all docker containers and images
#docker stop $(docker ps -a -q)
#docker rm $(docker ps -a -q)
#docker rmi -f $(docker images -aq)
#docker image prune -f

# Build and deploy of microservices
pushd messaging-utilities-3.4
mvn clean install
popd

pushd account-management
chmod +x mvnw
./mvnw package
popd

pushd payment-management
chmod +x mvnw
./mvnw package
popd

pushd token-management
chmod +x mvnw
./mvnw package
popd

pushd facade
chmod +x mvnw
./mvnw package
popd

docker image prune -f
docker-compose down --remove-orphans
docker-compose build
docker-compose up -d

# Give the Web server a chance to finish start up
sleep 2

# Run end-to-end tests
pushd test-client
mvn clean
mvn test
popd
