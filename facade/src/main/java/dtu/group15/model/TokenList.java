package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenList implements Serializable {
    private static final long serialVersionUID = 1L;
    ArrayList<Token> tokenList;
}
