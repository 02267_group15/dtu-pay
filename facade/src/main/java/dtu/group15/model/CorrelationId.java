package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CorrelationId implements Serializable {
    private static final long serialVersionUID = 1L;
    String id;

    public static CorrelationId randomId() {
        CorrelationId correlationId = new CorrelationId(UUID.randomUUID().toString());
        return correlationId;
    }
}
