package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManagerReport implements Serializable {
    private static final long serialVersionUID = 1L;
    Amount totalAmount;
    List<ManagerReportedPayment> payments;
}