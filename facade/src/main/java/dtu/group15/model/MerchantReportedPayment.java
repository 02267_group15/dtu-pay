package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MerchantReportedPayment implements Serializable {
    //Payment returned to merchant
    private static final long serialVersionUID = 1L;
    Token token;
    Amount amount;
}