package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerReportedPayment implements Serializable {
    //Payment returned to customer
    private static final long serialVersionUID = 1L;
    Id merchantId;
    Token token;
    Amount amount;
}