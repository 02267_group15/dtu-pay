/** @author Jens */
package dtu.group15.adapter.out;

import dtu.group15.model.*;

public interface IOutputPort {
    void customerRegistrationRequested(CorrelationId correlationId, BankId bankId);
    void customerDeregistrationRequested(CorrelationId correlationId, Id id);
    void customerReportRequested(CorrelationId correlationId, Id id);

    void managerReportRequested(CorrelationId correlationId);

    void merchantRegistrationRequested(CorrelationId correlationId, BankId bankId);
    void merchantDeregistrationRequested(CorrelationId correlationId, Id id);
    void merchantReportRequested(CorrelationId correlationId, Id id);

    void paymentRequested(CorrelationId correlationId, Id merchantId, Token token, Amount amount);

    void issueTokensRequested(CorrelationId correlationId, Id customerId, Amount amount);
}
