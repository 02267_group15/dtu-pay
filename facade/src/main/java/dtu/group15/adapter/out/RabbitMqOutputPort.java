/** @author Jens */
package dtu.group15.adapter.out;

import dtu.group15.events.outputs.*;
import dtu.group15.model.*;
import messaging.MessageQueue;

public class RabbitMqOutputPort implements IOutputPort{
    /**
     This class contains the methods for constructing the RabbitMq messages published from Facade.
     */

    private final MessageQueue messageQueue;

    public RabbitMqOutputPort(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    @Override
    public void customerRegistrationRequested(CorrelationId correlationId, BankId bankId) {
        this.messageQueue.publish(new CustomerRegistrationRequestedEvent(correlationId, bankId).asEvent());
    }

    @Override
    public void customerDeregistrationRequested(CorrelationId correlationId, Id id) {
        this.messageQueue.publish(new CustomerDeregistrationRequestedEvent(correlationId, id).asEvent());
    }

    @Override
    public void customerReportRequested(CorrelationId correlationId, Id id) {
        this.messageQueue.publish(new CustomerReportRequestedEvent(correlationId, id).asEvent());
    }

    @Override
    public void managerReportRequested(CorrelationId correlationId) {
        this.messageQueue.publish(new ManagerReportRequestedEvent(correlationId).asEvent());
    }

    @Override
    public void merchantRegistrationRequested(CorrelationId correlationId, BankId bankId) {
        this.messageQueue.publish(new MerchantRegistrationRequestedEvent(correlationId, bankId).asEvent());
    }

    @Override
    public void merchantDeregistrationRequested(CorrelationId correlationId, Id id) {
        this.messageQueue.publish(new MerchantDeregistrationRequestedEvent(correlationId, id).asEvent());
    }

    @Override
    public void merchantReportRequested(CorrelationId correlationId, Id id) {
        this.messageQueue.publish(new MerchantReportRequestedEvent(correlationId, id).asEvent());
    }

    @Override
    public void paymentRequested(CorrelationId correlationId, Id merchantId, Token token, Amount amount) {
        this.messageQueue.publish(new PaymentRequestedEvent(correlationId, merchantId, token, amount).asEvent());
    }

    @Override
    public void issueTokensRequested(CorrelationId correlationId, Id customerId, Amount amount) {
        this.messageQueue.publish(new IssueTokensRequestedEvent(correlationId, customerId, amount).asEvent());
    }
}
