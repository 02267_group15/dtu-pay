/** @author Aleksander */
package dtu.group15.adapter.in;

import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import dtu.group15.service.CustomerService;
import dtu.group15.service.TokenService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/customers")
public class CustomerResource {
    /**
    This class contains all REST end points used for customers.
     */

    CustomerService customerService;
    TokenService tokenService;

    public CustomerResource(CustomerService customerService, TokenService tokenService) {
        this.customerService = customerService;
        this.tokenService = tokenService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(Customer customer) {
        Logger.newRequest("Register request received");
        Customer registeredCustomer = customerService.register(customer.getBankId());
        Logger.log("Customer register finished");
        return Response.ok().entity(registeredCustomer).build();
    }

    @Path("/{id}/payments")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReport(@PathParam("id") String id) {
        Logger.newRequest("Get customer report request received");
        try {
            CustomerReport report = customerService.getReport(new Id(id));
            return Response.ok().entity(report).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deregister(@PathParam("id") String id) {
        Logger.newRequest("Deregister customer request received");
        try {
            customerService.deregister(new Id(id));
            return Response.ok().entity("Customer with id " + id + " has been deleted").build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }



    @Path("/{id}/tokens")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response issueTokens(TokenRequest tokenRequest) {
        Logger.newRequest("Issue tokens request received");
        try {
            TokenList tokenList = tokenService.requestIssueTokens(tokenRequest.getCustomerId(), tokenRequest.getAmount());
            return Response.ok().entity(tokenList).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }
}
