/** @author Jens */
package dtu.group15.adapter.in;

import dtu.group15.logger.Logger;
import dtu.group15.model.Id;
import dtu.group15.model.Merchant;
import dtu.group15.model.MerchantReport;
import dtu.group15.service.MerchantService;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/merchants")
public class MerchantResource {
    /**
     This class contains all REST end points used for merchants.
     */

    MerchantService merchantService;

    public MerchantResource(MerchantService merchantService) {
        this.merchantService = merchantService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response register(Merchant merchant) {
        Logger.newRequest("Register merchant request received");
        Merchant merchantResult = merchantService.register(merchant.getBankId());
        Logger.log("Register merchant finished");
        return Response.ok().entity(merchantResult).build();
    }

    @Path("/{id}/payments")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReport(@PathParam("id") String id) {
        Logger.newRequest("Get merchant report request received");
        try {
            MerchantReport report = merchantService.getReport(new Id(id));
            return Response.ok().entity(report).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }

    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deregister(@PathParam("id") String id) {
        Logger.newRequest("Deregister merchant request received");
        try {
            merchantService.deregister(new Id(id));
            return Response.ok().entity("Merchant with id " + id + " has been deregistered").build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }
}
