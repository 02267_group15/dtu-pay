/** @author Aleksander */
package dtu.group15.adapter.in;

import dtu.group15.events.inputs.*;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import dtu.group15.service.*;
import messaging.MessageQueue;

public class RabbitMqInputPort {
    /**
     This class contains the topics and handlers used in Facade.
     */

    private final CustomerService customerService;
    private final ManagerService managerService;
    private final MerchantService merchantService;
    private final PaymentService paymentService;
    private final TokenService tokenService;

    public RabbitMqInputPort(MessageQueue messageQueue, CustomerService customerService, ManagerService managerService, MerchantService merchantService, PaymentService paymentService, TokenService tokenService) {
        this.customerService = customerService;
        this.managerService = managerService;
        this.merchantService = merchantService;
        this.paymentService = paymentService;
        this.tokenService = tokenService;

        messageQueue.addHandler(CustomerRegisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerRegisteredEvent.class)));
        messageQueue.addHandler(CustomerDeregisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerDeregisteredEvent.class)));
        messageQueue.addHandler(CustomerNotDeregisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerNotDeregisteredEvent.class)));
        messageQueue.addHandler(CustomerReportProcessedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerReportProcessedEvent.class)));
        messageQueue.addHandler(CustomerDidNotExistEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerDidNotExistEvent.class)));

        messageQueue.addHandler(ManagerReportProcessedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, ManagerReportProcessedEvent.class)));

        messageQueue.addHandler(MerchantRegisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantRegisteredEvent.class)));
        messageQueue.addHandler(MerchantDeregisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantDeregisteredEvent.class)));
        messageQueue.addHandler(MerchantNotDeregisteredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantNotDeregisteredEvent.class)));
        messageQueue.addHandler(MerchantReportProcessedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantReportProcessedEvent.class)));
        messageQueue.addHandler(MerchantDidNotExistEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantDidNotExistEvent.class)));

        messageQueue.addHandler(PaymentProcessedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, PaymentProcessedEvent.class)));
        messageQueue.addHandler(PaymentFailedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, PaymentFailedEvent.class)));

        messageQueue.addHandler(TokensIssuedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, TokensIssuedEvent.class)));
        messageQueue.addHandler(TokensNotIssuedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, TokensNotIssuedEvent.class)));
        messageQueue.addHandler(TokenNotValidatedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, TokenNotValidatedEvent.class)));
        messageQueue.addHandler(MerchantBankIDNotAcquiredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantBankIDNotAcquiredEvent.class)));
    }

    private void apply(CustomerRegisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getId();
        BankId bankId = e.getBankId();

        this.customerService.handleCustomerRegistrationSuccess(correlationId, customerId, bankId);
    }

    private void apply(CustomerDeregisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();

        this.customerService.handleCustomerDeregistrationSuccess(correlationId);
    }

    private void apply(CustomerNotDeregisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.customerService.handleCustomerDeregistrationFailed(correlationId, errorMessage);
    }

    private void apply(CustomerReportProcessedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        CustomerReport customerReport = e.getCustomerReport();

        this.customerService.handleCustomerReportSuccess(correlationId, customerReport);
    }

    private void apply(CustomerDidNotExistEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();

        String errorMessage = "The customer is not registered in DTU Pay";
        this.customerService.handleCustomerDidNotExist(correlationId, errorMessage);
        this.tokenService.handleCustomerDidNotExist(correlationId, errorMessage);
    }

    private void apply(ManagerReportProcessedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        ManagerReport managerReport = e.getManagerReport();

        this.managerService.handleManagerReportSuccess(correlationId, managerReport);
    }

    private void apply(MerchantRegisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        Id merchantId = e.getId();
        BankId bankId = e.getBankId();

        this.merchantService.handleMerchantRegistrationSuccess(correlationId, merchantId, bankId);
    }

    private void apply(MerchantDeregisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();

        this.merchantService.handleMerchantDeregistrationSuccess(correlationId);
    }

    private void apply(MerchantNotDeregisteredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.merchantService.handleMerchantDeregistrationFailed(correlationId, errorMessage);
    }

    private void apply(MerchantReportProcessedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        MerchantReport merchantReport = e.getMerchantReport();

        this.merchantService.handleMerchantReportSuccess(correlationId, merchantReport);
    }

    private void apply(MerchantDidNotExistEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();

        this.merchantService.handleMerchantReportFailed(correlationId, "The merchant is not registered in DTU Pay");
    }

    private void apply(PaymentProcessedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();

        this.paymentService.handlePaymentSuccess(correlationId, e.getMerchantId(), e.getToken(), e.getAmount());
    }

    private void apply(PaymentFailedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.paymentService.handlePaymentFailed(correlationId, errorMessage);
    }

    private void apply(TokensIssuedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        TokenList tokenList = e.getTokenList();

        this.tokenService.handleIssueTokensSuccess(correlationId, tokenList);
    }

    private void apply(TokensNotIssuedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.tokenService.handleIssueTokensFailed(correlationId, errorMessage);
    }

    private void apply(TokenNotValidatedEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.paymentService.handlePaymentFailed(correlationId, errorMessage);
    }

    private void apply(MerchantBankIDNotAcquiredEvent e) {
        Logger.log(e.getType() + " received");
        CorrelationId correlationId = e.getCorrelationId();
        String errorMessage = e.getErrorMessage();

        this.paymentService.handlePaymentFailed(correlationId, errorMessage);
    }
}
