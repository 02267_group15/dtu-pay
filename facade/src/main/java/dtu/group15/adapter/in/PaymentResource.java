/** @author Michael */
package dtu.group15.adapter.in;

import dtu.group15.logger.Logger;
import dtu.group15.model.ManagerReport;
import dtu.group15.model.MerchantPayment;
import dtu.group15.service.ManagerService;
import dtu.group15.service.PaymentService;
import dtu.group15.service.ServiceFactory;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/payments")
public class PaymentResource {
    /**
     This class contains all REST end points used for payments.
     */

    PaymentService paymentService;
    ManagerService managerService;

    public PaymentResource(PaymentService paymentService, ManagerService managerService) {
        this.paymentService = paymentService;
        this.managerService = managerService;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response pay(MerchantPayment merchantPayment) {
        Logger.newRequest("Payment request received");
        try {
            MerchantPayment paymentResult = paymentService.pay(merchantPayment.getMerchantId(), merchantPayment.getToken(), merchantPayment.getAmount());
            return Response.ok().entity(paymentResult).build();
        } catch (Exception e) {
            return Response.status(Response.Status.CONFLICT).entity(e.getMessage()).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getReport() {
        Logger.newRequest("Get manager report request received");
        ManagerReport report = managerService.getReport();
        Logger.log("Manager report finished");
        return Response.ok().entity(report).build();
    }
}
