package dtu.group15.events;

import messaging.Event;

public class DTUPayEvent {
    public String getType() {
        return this.getClass().getSimpleName();
    }

    public Event asEvent() {
        return new Event(this.getType(), new Object[]{this});
    }
}
