package dtu.group15.events.outputs;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Value;
import messaging.Event;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PaymentRequestedEvent extends DTUPayEvent {
    private static final long serialVersionUID = 1L;
    CorrelationId correlationId;
    Id merchantId;
    Token token;
    Amount amount;
}
