package dtu.group15.events.inputs;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.CorrelationId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantBankIDNotAcquiredEvent extends DTUPayEvent {
    private static final long serialVersionUID = 1L;
    CorrelationId correlationId;
    String errorMessage;
}
