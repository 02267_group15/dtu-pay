package dtu.group15.startup;

import dtu.group15.adapter.in.RabbitMqInputPort;
import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.adapter.out.RabbitMqOutputPort;
import dtu.group15.events.outputs.CustomerRegistrationRequestedEvent;
import dtu.group15.logger.Logger;
import dtu.group15.service.*;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.Startup;
import io.quarkus.runtime.annotations.QuarkusMain;
import messaging.implementations.RabbitMqQueue;

import java.security.Provider;

@QuarkusMain
public class Main {
    public final static String RABBIT_MQ_HOST = "rabbit-mq";

    public static void main(String[] args) {
        Quarkus.run(StartUp.class, args);
    }

    public static class StartUp implements QuarkusApplication {
        @Override
        public int run(String... args) throws Exception {
            Logger.start();
            var mq = new RabbitMqQueue(RABBIT_MQ_HOST);

            var customerService = ServiceFactory.getCustomerService();
            var managerService = ServiceFactory.getManagerService();
            var merchantService = ServiceFactory.getMerchantService();
            var paymentService = ServiceFactory.getPaymentService();
            var tokenService = ServiceFactory.getTokenService();

            new RabbitMqInputPort(mq, customerService, managerService, merchantService, paymentService, tokenService);

            Quarkus.waitForExit();
            return 0;
        }
    }
}
