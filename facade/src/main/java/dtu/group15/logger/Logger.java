package dtu.group15.logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class Logger {
    private static String filePath = "log.txt";
    private static int requestNumber = 1;
    public static void start() {
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        try {
            file.createNewFile();
        } catch (Exception ignored) {ignored.printStackTrace();};
    }

    public static void log(String string) {
        try {
            FileWriter fw = new FileWriter(filePath, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(string);
            bw.newLine();
            bw.close();
        } catch (Exception ignored) {ignored.printStackTrace();}
    }

    public static void newRequest(String string) {
        log("\n"+requestNumber + ". " + string);
        requestNumber++;
    }
}
