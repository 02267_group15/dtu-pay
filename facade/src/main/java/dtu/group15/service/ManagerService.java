/** @author Jens */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Customer;
import dtu.group15.model.ManagerReport;
import dtu.group15.model.Merchant;
import jakarta.ws.rs.core.Response;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class ManagerService {
    IOutputPort outputPort;
    private Map<CorrelationId, CompletableFuture<ManagerReport>> correlations = new ConcurrentHashMap<>();

    public ManagerService(IOutputPort outputPort) {
        this.outputPort = outputPort;
    }

    //Sends a manager report request to the message queue and waits for a response.
    public ManagerReport getReport() {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<ManagerReport> managerReport = new CompletableFuture<>();
        correlations.put(correlationId, managerReport);

        this.outputPort.managerReportRequested(correlationId);

        return managerReport.orTimeout(10, TimeUnit.SECONDS).join();
    }

    //Successful completion of a manager report request.
    public void handleManagerReportSuccess(CorrelationId correlationId, ManagerReport managerReport) {
        correlations.remove(correlationId).complete(managerReport);
    }
}
