/** @author Christian */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.adapter.out.RabbitMqOutputPort;
import dtu.group15.startup.Main;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import messaging.MessageQueue;
import messaging.implementations.RabbitMqQueue;

@ApplicationScoped
public class ServiceFactory {
    /**
     * Class for factorizing the RabbitMq message queue, output port and service classes.
     * At startup, services will be dependency injected automatically where possible
     */

    private static CustomerService customerService;
    private static MerchantService merchantService;
    private static PaymentService paymentService;
    private static TokenService tokenService;
    private static ManagerService managerService;

    @Produces
    public static synchronized CustomerService getCustomerService() {
        if(customerService != null) {
            return customerService;
        }

        MessageQueue mq = new RabbitMqQueue(Main.RABBIT_MQ_HOST);
        IOutputPort outputPort = new RabbitMqOutputPort(mq);
        customerService = new CustomerService(outputPort);
        return customerService;
    }

    @Produces
    public static synchronized MerchantService getMerchantService() {
        if(merchantService != null) {
            return merchantService;
        }

        MessageQueue mq = new RabbitMqQueue(Main.RABBIT_MQ_HOST);
        IOutputPort outputPort = new RabbitMqOutputPort(mq);
        merchantService = new MerchantService(outputPort);
        return merchantService;
    }

    @Produces
    public static synchronized PaymentService getPaymentService() {
        if(paymentService != null) {
            return paymentService;
        }

        MessageQueue mq = new RabbitMqQueue(Main.RABBIT_MQ_HOST);
        IOutputPort outputPort = new RabbitMqOutputPort(mq);
        paymentService = new PaymentService(outputPort);
        return paymentService;
    }

    @Produces
    public static synchronized TokenService getTokenService() {
        if(tokenService != null) {
            return tokenService;
        }

        MessageQueue mq = new RabbitMqQueue(Main.RABBIT_MQ_HOST);
        IOutputPort outputPort = new RabbitMqOutputPort(mq);
        tokenService = new TokenService(outputPort);
        return tokenService;
    }

    @Produces
    public static synchronized ManagerService getManagerService() {
        if(managerService != null) {
            return managerService;
        }

        MessageQueue mq = new RabbitMqQueue(Main.RABBIT_MQ_HOST);
        IOutputPort outputPort = new RabbitMqOutputPort(mq);
        managerService = new ManagerService(outputPort);
        return managerService;
    }
}
