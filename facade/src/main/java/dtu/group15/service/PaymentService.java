/** @author Nicolai */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import jakarta.ws.rs.core.Response;
import messaging.Event;
import messaging.MessageQueue;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class PaymentService {
    IOutputPort outputPort;

    private Map<CorrelationId, CompletableFuture<MerchantPayment>> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, String> errorMessageCorrelations = new ConcurrentHashMap<>();

    public PaymentService(IOutputPort outputPort) {
        this.outputPort = outputPort;
    }

    //Sends a payment request to the message queue and waits for a response.
    public MerchantPayment pay(Id merchantId, Token token, Amount amount) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<MerchantPayment> registeredMerchantPayment = new CompletableFuture<>();

        this.correlations.put(correlationId, registeredMerchantPayment);
        this.outputPort.paymentRequested(correlationId, merchantId, token, amount);

        MerchantPayment payment = registeredMerchantPayment.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Payment finished");
        if (payment == null) {
            String errorMessage = errorMessageCorrelations.remove(correlationId);
            throw new Exception(errorMessage);
        }
        return payment;
    }

        //Successful completion of a payment request.
    public void handlePaymentSuccess(CorrelationId correlationId, Id merchantId, Token token, Amount amount) {
        MerchantPayment merchantPayment = new MerchantPayment(merchantId, token, amount);
        correlations.remove(correlationId).complete(merchantPayment);
    }

    //Failed completion of a payment request.
    public void handlePaymentFailed(CorrelationId correlationId, String errorMessage) {
        errorMessageCorrelations.put(correlationId, errorMessage);
        correlations.remove(correlationId).complete(null);
    }
}
