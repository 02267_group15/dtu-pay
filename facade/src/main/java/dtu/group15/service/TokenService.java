/** @author Magnus */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class TokenService {
    IOutputPort outputPort;

    private Map<CorrelationId, CompletableFuture> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, String> errorMessageCorrelations = new ConcurrentHashMap<>();

    public TokenService(IOutputPort outputPort) {
        this.outputPort = outputPort;
    }

    //Sends a request for issuing tokens to the message queue and waits for a response.
    public TokenList requestIssueTokens(Id customerId, Amount amount) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<TokenList> issuedTokens = new CompletableFuture<>();

        this.correlations.put(correlationId, issuedTokens);
        this.outputPort.issueTokensRequested(correlationId, customerId, amount);

        TokenList tokenList = issuedTokens.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Issue tokens finished");
        if (tokenList == null) {
            throw new Exception(errorMessageCorrelations.get(correlationId));
        }
        return tokenList;
    }

    //Successful completion of an issue token(s) request.
    public void handleIssueTokensSuccess(CorrelationId correlationId, TokenList issuedTokens) {
        correlations.remove(correlationId).complete(issuedTokens);
    }

    //Failed completion of an issue token(s) request.
    public void handleIssueTokensFailed(CorrelationId correlationId, String errorMessage) {
        errorMessageCorrelations.put(correlationId, errorMessage);
        correlations.remove(correlationId).complete(null);
    }

    //Failed completion of an issue token(s) request due to customer not being registered in DTU Pay.
    public void handleCustomerDidNotExist(CorrelationId correlationId, String errorMessage) {
        if (correlations.containsKey(correlationId)) {
            errorMessageCorrelations.put(correlationId, errorMessage);
            correlations.get(correlationId).complete(null);
        }
    }

}
