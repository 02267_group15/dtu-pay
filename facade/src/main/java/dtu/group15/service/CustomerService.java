/** @author Aleksander */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class CustomerService {
    IOutputPort outputPort;
    private Map<CorrelationId, CompletableFuture> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, String> errorMessageCorrelations = new ConcurrentHashMap<>();

    public CustomerService(IOutputPort outputPort) {
        this.outputPort = outputPort;
    }

    //Sends a customer registration request to the message queue and waits for a response.
    public Customer register(BankId bankId) {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<Customer> registeredCustomer = new CompletableFuture<>();

        this.correlations.put(correlationId, registeredCustomer);
        this.outputPort.customerRegistrationRequested(correlationId, bankId);

        return registeredCustomer.orTimeout(10, TimeUnit.SECONDS).join();
    }

    //Sends a customer deregistration request to the message queue and waits for a response.
    public void deregister(Id id) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<Void> deregisteredId = new CompletableFuture<>();

        this.correlations.put(correlationId, deregisteredId);
        this.outputPort.customerDeregistrationRequested(correlationId, id);

        deregisteredId.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Deregister customer finished");
        String errorMessage = errorMessageCorrelations.remove(correlationId);
        if (errorMessage != null) {
            throw new Exception(errorMessage);
        }
    }

    //Sends a customer report request to the message queue and waits for a response.
    public CustomerReport getReport(Id id) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<CustomerReport> reportFuture = new CompletableFuture<>();

        this.correlations.put(correlationId, reportFuture);
        this.outputPort.customerReportRequested(correlationId, id);

        CustomerReport report = reportFuture.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Customer report finished");
        if (report == null) {
            throw new Exception(errorMessageCorrelations.remove(correlationId));
        }
        return report;
    }

    //Successful completion of a customer registration request.
    public void handleCustomerRegistrationSuccess(CorrelationId correlationId, Id customerId, BankId bankId) {
        correlations.get(correlationId).complete(new Customer(customerId, bankId));
    }

    //Successful completion of a customer deregistration request.
    public void handleCustomerDeregistrationSuccess(CorrelationId correlationId) {
        correlations.get(correlationId).complete(null);
    }

    //Failed completion of a customer deregistration request.
    public void handleCustomerDeregistrationFailed(CorrelationId correlationId, String errorMessage) {
        errorMessageCorrelations.put(correlationId, errorMessage);
        correlations.get(correlationId).complete(null);
    }

    //Successful completion of a customer report request.
    public void handleCustomerReportSuccess(CorrelationId correlationId, CustomerReport customerReport) {
        correlations.get(correlationId).complete(customerReport);
    }

    //Failed completion of a customer when the customer does not exist in DTU Pay.
    public void handleCustomerDidNotExist(CorrelationId correlationId, String errorMessage) {
        if (correlations.containsKey(correlationId)) {
            errorMessageCorrelations.put(correlationId, errorMessage);
            correlations.get(correlationId).complete(null);
        }
    }
}
