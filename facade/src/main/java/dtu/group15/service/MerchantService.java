/** @author Michael */
package dtu.group15.service;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class MerchantService {
    IOutputPort outputPort;

    private Map<CorrelationId, CompletableFuture> correlations = new ConcurrentHashMap<>();
    private Map<CorrelationId, String> errorMessageCorrelations = new ConcurrentHashMap<>();

    public MerchantService(IOutputPort outputPort) {
        this.outputPort = outputPort;
    }

    //Sends a merchant registration request to the message queue and waits for a response.
    public Merchant register(BankId bankId) {
        CorrelationId correlationId = CorrelationId.randomId();
        Merchant merchant = new Merchant();
        merchant.setBankId(bankId);
        CompletableFuture<Merchant> registeredMerchant = new CompletableFuture<>();

        this.correlations.put(correlationId, registeredMerchant);
        this.outputPort.merchantRegistrationRequested(correlationId, bankId);

        return registeredMerchant.orTimeout(10, TimeUnit.SECONDS).join();
    }

    //Sends a merchant deregistration request to the message queue and waits for a response.
    public void deregister(Id id) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<Void> deregisteredId = new CompletableFuture<>();

        this.correlations.put(correlationId, deregisteredId);
        this.outputPort.merchantDeregistrationRequested(correlationId, id);

        deregisteredId.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Deregister merchant finished");
        if (errorMessageCorrelations.containsKey(correlationId)) {
            String errorMessage = errorMessageCorrelations.remove(correlationId);
            throw new Exception(errorMessage);
        }
    }

    //Sends a merchant report request to the message queue and waits for a response.
    public MerchantReport getReport(Id id) throws Exception {
        CorrelationId correlationId = CorrelationId.randomId();
        CompletableFuture<MerchantReport> reportFuture = new CompletableFuture<>();

        correlations.put(correlationId, reportFuture);
        this.outputPort.merchantReportRequested(correlationId, id);

        MerchantReport report = reportFuture.orTimeout(10, TimeUnit.SECONDS).join();
        Logger.log("Merchant report finished");
        if (errorMessageCorrelations.containsKey(correlationId)) {
            throw new Exception(errorMessageCorrelations.get(correlationId));
        }
        return report;
    }

    //Successful completion of a merchant registration request.
    public void handleMerchantRegistrationSuccess(CorrelationId correlationId, Id merchantId, BankId bankId) {
        correlations.get(correlationId).complete(new Merchant(merchantId, bankId));
    }

    //Successful completion of a merchant deregistration request.
    public void handleMerchantDeregistrationSuccess(CorrelationId correlationId) {
        correlations.get(correlationId).complete(null);
    }

    //Failed completion of a merchant deregistration request.
    public void handleMerchantDeregistrationFailed(CorrelationId correlationId, String errorMessage) {
        errorMessageCorrelations.put(correlationId, errorMessage);
        correlations.get(correlationId).complete(null);
    }

    //Successful completion of a merchant report request.
    public void handleMerchantReportSuccess(CorrelationId correlationId, MerchantReport merchantReport) {
        correlations.get(correlationId).complete(merchantReport);
    }

    //Failed completion of a merchant report request.
    public void handleMerchantReportFailed(CorrelationId correlationId, String errorMessage) {
        errorMessageCorrelations.put(correlationId, errorMessage);
        correlations.get(correlationId).complete(null);
    }
}
