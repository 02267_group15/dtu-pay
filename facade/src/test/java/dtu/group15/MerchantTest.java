/** @author Magnus */
package dtu.group15;

import dtu.group15.events.inputs.*;
import dtu.group15.events.outputs.MerchantDeregistrationRequestedEvent;
import dtu.group15.events.outputs.MerchantRegistrationRequestedEvent;
import dtu.group15.events.outputs.MerchantReportRequestedEvent;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class MerchantTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void successfulRegisterMerchantTest() {
        CompletableFuture<Merchant> registeredMerchant = new CompletableFuture<>();

        // Create merchant to register
        Merchant merchantToRegister = new Merchant();
        merchantToRegister.setBankId(new BankId("123"));

        // Wait for merchant to be registered
        new Thread(() -> {
            var result = TestUtil.merchantService.register(merchantToRegister.getBankId());
            registeredMerchant.complete(result);
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, MerchantRegistrationRequestedEvent.class).getCorrelationId();
        var expected = new MerchantRegistrationRequestedEvent(correlationId, merchantToRegister.getBankId());
        assertEquals(expected, capturedEvent.getArgument(0, MerchantRegistrationRequestedEvent.class));

        // Check if handler handles
        Event event = new MerchantRegisteredEvent(correlationId, merchantToRegister.getId(), merchantToRegister.getBankId()).asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantRegisteredEvent.class);
        assertEquals(merchantToRegister, registeredMerchant.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void successfulDeRegisterMerchantTest() {
        CompletableFuture<Void> result = new CompletableFuture<>();

        // Create merchant to Deregister
        Merchant merchantToDeRegister = new Merchant();
        merchantToDeRegister.setId(new Id("123"));

        // Wait for merchant to be registered
        new Thread(() -> {
            try {
                TestUtil.merchantService.deregister(merchantToDeRegister.getId());
                result.complete(null);
            } catch (Exception ignored) {
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, MerchantDeregistrationRequestedEvent.class).getCorrelationId();
        var expected = new MerchantDeregistrationRequestedEvent(correlationId, merchantToDeRegister.getId());
        assertEquals(expected, capturedEvent.getArgument(0, MerchantDeregistrationRequestedEvent.class));

        Event event = new MerchantDeregisteredEvent(correlationId).asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantDeregisteredEvent.class);
        result.orTimeout(5, TimeUnit.SECONDS).join();
    }

    @Test
    public void failedDeregisterMerchantTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        // Create merchant to Deregister
        Merchant merchantToDeRegister = new Merchant();
        merchantToDeRegister.setId(new Id("123"));

        // Wait for merchant to be registered
        new Thread(() -> {
            try {
                TestUtil.merchantService.deregister(merchantToDeRegister.getId());
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, MerchantDeregistrationRequestedEvent.class).getCorrelationId();
        var expected = new MerchantDeregistrationRequestedEvent(correlationId, merchantToDeRegister.getId());
        assertEquals(expected, capturedEvent.getArgument(0, MerchantDeregistrationRequestedEvent.class));

        Event event = new MerchantNotDeregisteredEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantNotDeregisteredEvent.class);
        String error = result.orTimeout(5, TimeUnit.SECONDS).join();
        assertEquals("error", error);
    }

    @Test
    public void successfulGetReportMerchantTest() {
        CompletableFuture<MerchantReport> report = new CompletableFuture<>();

        new Thread(() -> {
            try {
               var result = TestUtil.merchantService.getReport(new Id("123"));
               report.complete(result);
            } catch (Exception ignored) {
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, MerchantReportRequestedEvent.class).getCorrelationId();
        var expected = new MerchantReportRequestedEvent(correlationId, new Id("123"));
        assertEquals(expected, capturedEvent.getArgument(0, MerchantReportRequestedEvent.class));

        Event event = new MerchantReportProcessedEvent(correlationId, new MerchantReport()).asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantReportProcessedEvent.class);
        assertNotNull(report.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void failedGetReportMerchantTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        new Thread(() -> {
            try {
                TestUtil.merchantService.getReport(new Id("123"));
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, MerchantReportRequestedEvent.class).getCorrelationId();
        var expected = new MerchantReportRequestedEvent(correlationId, new Id("123"));
        assertEquals(expected, capturedEvent.getArgument(0, MerchantReportRequestedEvent.class));

        Event event = new MerchantDidNotExistEvent(correlationId, null).asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantDidNotExistEvent.class);
        assertEquals("The merchant is not registered in DTU Pay", result.orTimeout(5, TimeUnit.SECONDS).join());
    }
}
