/** @author Christian */
package dtu.group15;

import dtu.group15.adapter.out.IOutputPort;
import dtu.group15.adapter.out.RabbitMqOutputPort;
import dtu.group15.events.inputs.ManagerReportProcessedEvent;
import dtu.group15.events.outputs.ManagerReportRequestedEvent;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.ManagerReport;
import dtu.group15.service.ManagerService;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class ManagerTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void successfulGetReportManagerTest() {
        CompletableFuture<ManagerReport> report = new CompletableFuture<>();

        new Thread(() -> {
            try {
                var result = TestUtil.managerService.getReport();
                report.complete(result);
            } catch (Exception ignored) {}
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, ManagerReportRequestedEvent.class).getCorrelationId();
        var expected = new ManagerReportRequestedEvent(correlationId);
        assertEquals(expected, capturedEvent.getArgument(0, ManagerReportRequestedEvent.class));

        Event event = new ManagerReportProcessedEvent(correlationId, new ManagerReport()).asEvent();
        TestUtil.publishEvent(consumerMap, event, ManagerReportProcessedEvent.class);
        assertNotNull(report.orTimeout(5, TimeUnit.SECONDS).join());
    }
}
