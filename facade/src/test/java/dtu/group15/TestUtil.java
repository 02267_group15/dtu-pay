/** @author Nicolai */
package dtu.group15;

import dtu.group15.adapter.in.RabbitMqInputPort;
import dtu.group15.adapter.out.RabbitMqOutputPort;
import dtu.group15.service.*;
import messaging.Event;
import messaging.MessageQueue;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;

public class TestUtil {
    private static MessageQueue messageQueue;
    public static CustomerService customerService;
    public static ManagerService managerService;
    public static MerchantService merchantService;
    public static PaymentService paymentService;
    public static TokenService tokenService;
    private static CompletableFuture waitForEventToGetAdded = null;

    public static Event awaitEvent(List<Event> capturedEvents) {
        CompletableFuture completableFuture = createCompletableFuture();
        completableFuture.orTimeout(5, TimeUnit.SECONDS).join();
        waitForEventToGetAdded = null;
        Event capturedEvent = capturedEvents.removeFirst();
        assertNotNull(capturedEvent);
        return capturedEvent;
    }

    private static synchronized CompletableFuture createCompletableFuture() {
        if (waitForEventToGetAdded == null) {
            waitForEventToGetAdded = new CompletableFuture();
        }
        return waitForEventToGetAdded;
    }

    public static Map<String, List<Consumer<Event>>> mapConsumersToTopics(MessageQueue queue) {
        Map<String, List<Consumer<Event>>> consumerMap = new ConcurrentHashMap<>();
        Mockito.doAnswer(invocation -> {
            String topic = invocation.getArgument(0);
            Consumer<Event> consumer = invocation.getArgument(1);
            if (consumerMap.containsKey(topic)) {
                consumerMap.get(topic).add(consumer);
            } else {
                consumerMap.put(topic, new ArrayList<>() {{ add(consumer); }});
            }
            return null;
        }).when(queue).addHandler(Mockito.anyString(), Mockito.any());
        return consumerMap;
    }

    public static void initializeInputOutputPorts(MessageQueue queue) {
        messageQueue = queue;
        RabbitMqOutputPort outputPort = new RabbitMqOutputPort(queue);
        customerService = new CustomerService(outputPort);
        managerService = new ManagerService(outputPort);
        merchantService = new MerchantService(outputPort);
        paymentService = new PaymentService(outputPort);
        tokenService = new TokenService(outputPort);
        new RabbitMqInputPort(queue, customerService, managerService, merchantService, paymentService, tokenService);
    }

    public static List<Event> initializeEventPuller(MessageQueue queue) {
        List<Event> capturedEvents = new ArrayList<>();
        Mockito.doAnswer(invocation -> {
            waitForEventToGetAdded = createCompletableFuture();
            Event event = invocation.getArgument(0);
            capturedEvents.add(event);
            waitForEventToGetAdded.complete(null);
            return null;
        }).when(queue).publish(Mockito.any());

        return capturedEvents;
    }

    public static void publishEvent(Map<String, List<Consumer<Event>>> consumerMap, Event event, Class c) {
        consumerMap.get(c.getSimpleName()).forEach(consumer -> consumer.accept(event));
    }
}
