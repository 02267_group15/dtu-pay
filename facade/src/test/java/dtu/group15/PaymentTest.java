/** @author Aleksander */
package dtu.group15;

import dtu.group15.events.inputs.MerchantBankIDNotAcquiredEvent;
import dtu.group15.events.inputs.PaymentFailedEvent;
import dtu.group15.events.inputs.PaymentProcessedEvent;
import dtu.group15.events.inputs.TokenNotValidatedEvent;
import dtu.group15.events.outputs.PaymentRequestedEvent;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import dtu.group15.model.MerchantPayment;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class PaymentTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void successfulPaymentTest() {
        CompletableFuture<MerchantPayment> paymentFuture = new CompletableFuture<>();

        // Create customer to register
        MerchantPayment paymentToRegister = new MerchantPayment();
        paymentToRegister.setMerchantId(new Id("123"));

        // Wait for customer to be registered
        new Thread(() -> {
            try {
                var result = TestUtil.paymentService.pay(paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
                paymentFuture.complete(result);
            } catch (Exception ignored) {}
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, PaymentRequestedEvent.class).getCorrelationId();
        var expected = new PaymentRequestedEvent(correlationId, paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, PaymentRequestedEvent.class));

        // Check if handler handles
        Event event = new PaymentProcessedEvent(correlationId, paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount()).asEvent();
        TestUtil.publishEvent(consumerMap, event, PaymentProcessedEvent.class);
        assertEquals(paymentToRegister, paymentFuture.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void paymentFailedTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        // Create customer to register
        MerchantPayment paymentToRegister = new MerchantPayment();
        paymentToRegister.setMerchantId(new Id("123"));

        // Wait for customer to be registered
        new Thread(() -> {
            try {
                TestUtil.paymentService.pay(paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, PaymentRequestedEvent.class).getCorrelationId();
        var expected = new PaymentRequestedEvent(correlationId, paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, PaymentRequestedEvent.class));

        // Check if handler handles
        Event event = new PaymentFailedEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, PaymentFailedEvent.class);
        String error = result.orTimeout(5, TimeUnit.SECONDS).join();
        assertEquals("error", error);
    }

    @Test
    public void paymentFailedMerchantNotAcquiredTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        // Create customer to register
        MerchantPayment paymentToRegister = new MerchantPayment();
        paymentToRegister.setMerchantId(new Id("123"));

        // Wait for customer to be registered
        new Thread(() -> {
            try {
                TestUtil.paymentService.pay(paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, PaymentRequestedEvent.class).getCorrelationId();
        var expected = new PaymentRequestedEvent(correlationId, paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, PaymentRequestedEvent.class));

        // Check if handler handles
        Event event = new MerchantBankIDNotAcquiredEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, MerchantBankIDNotAcquiredEvent.class);
        String error = result.orTimeout(5, TimeUnit.SECONDS).join();
        assertEquals("error", error);
    }

    @Test
    public void paymentFailedTokenNotValidatedTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        // Create customer to register
        MerchantPayment paymentToRegister = new MerchantPayment();
        paymentToRegister.setMerchantId(new Id("123"));

        // Wait for customer to be registered
        new Thread(() -> {
            try {
                TestUtil.paymentService.pay(paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, PaymentRequestedEvent.class).getCorrelationId();
        var expected = new PaymentRequestedEvent(correlationId, paymentToRegister.getMerchantId(), paymentToRegister.getToken(), paymentToRegister.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, PaymentRequestedEvent.class));

        // Check if handler handles
        Event event = new TokenNotValidatedEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, TokenNotValidatedEvent.class);
        String error = result.orTimeout(5, TimeUnit.SECONDS).join();
        assertEquals("error", error);
    }
}
