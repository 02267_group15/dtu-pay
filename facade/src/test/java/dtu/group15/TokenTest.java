/** @author Jens */
package dtu.group15;

import dtu.group15.events.inputs.CustomerDidNotExistEvent;
import dtu.group15.events.inputs.TokensIssuedEvent;
import dtu.group15.events.inputs.TokensNotIssuedEvent;
import dtu.group15.events.outputs.IssueTokensRequestedEvent;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class TokenTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void successfulIssueTokensTest() {
        CompletableFuture<TokenList> issuedTokens = new CompletableFuture<>();

        // Create token request
        TokenRequest tokenRequest = new TokenRequest();

        // Wait for returned token list
        new Thread(() -> {
            try {
                TokenList result = TestUtil.tokenService.requestIssueTokens(tokenRequest.getCustomerId(), tokenRequest.getAmount());
                issuedTokens.complete(result);
            } catch (Exception ignored) {}
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, IssueTokensRequestedEvent.class).getCorrelationId();
        var expected = new IssueTokensRequestedEvent(correlationId, tokenRequest.getCustomerId(), tokenRequest.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, IssueTokensRequestedEvent.class));

        // Check if handler handles
        TokenList expectedTokenList = new TokenList();
        Event event = new TokensIssuedEvent(correlationId, expectedTokenList).asEvent();
        TestUtil.publishEvent(consumerMap, event, TokensIssuedEvent.class);
        assertEquals(expectedTokenList, issuedTokens.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void failedIssueTokensTest() {
        CompletableFuture<String> issuedTokens = new CompletableFuture<>();

        // Create token request
        TokenRequest tokenRequest = new TokenRequest();

        // Wait for returned token list
        new Thread(() -> {
            try {
                TestUtil.tokenService.requestIssueTokens(tokenRequest.getCustomerId(), tokenRequest.getAmount());
            } catch (Exception e) {
                issuedTokens.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, IssueTokensRequestedEvent.class).getCorrelationId();
        var expected = new IssueTokensRequestedEvent(correlationId, tokenRequest.getCustomerId(), tokenRequest.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, IssueTokensRequestedEvent.class));

        // Check if handler handles
        Event event = new TokensNotIssuedEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, TokensNotIssuedEvent.class);
        assertEquals("error", issuedTokens.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void issueTokensCustomerDoesNotExistTest() {
        CompletableFuture<String> issuedTokens = new CompletableFuture<>();

        // Create token request
        TokenRequest tokenRequest = new TokenRequest();

        // Wait for returned token list
        new Thread(() -> {
            try {
                TestUtil.tokenService.requestIssueTokens(tokenRequest.getCustomerId(), tokenRequest.getAmount());
            } catch (Exception e) {
                issuedTokens.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, IssueTokensRequestedEvent.class).getCorrelationId();
        var expected = new IssueTokensRequestedEvent(correlationId, tokenRequest.getCustomerId(), tokenRequest.getAmount());
        assertEquals(expected, capturedEvent.getArgument(0, IssueTokensRequestedEvent.class));

        // Check if handler handles
        Event event = new CustomerDidNotExistEvent(correlationId, null).asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerDidNotExistEvent.class);
        assertEquals("The customer is not registered in DTU Pay", issuedTokens.orTimeout(5, TimeUnit.SECONDS).join());
    }
}
