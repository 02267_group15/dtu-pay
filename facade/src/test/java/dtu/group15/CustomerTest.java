/** @author Nicolai */
package dtu.group15;

import dtu.group15.events.inputs.*;
import dtu.group15.events.outputs.CustomerDeregistrationRequestedEvent;
import dtu.group15.events.outputs.CustomerRegistrationRequestedEvent;
import dtu.group15.events.outputs.CustomerReportRequestedEvent;
import dtu.group15.model.*;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

public class CustomerTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private List<Event> capturedEvents;

    @Before
    public void initializeAll() {
        MessageQueue queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        TestUtil.initializeInputOutputPorts(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue);
    }

    @Test
    public void successfulRegisterCustomerTest() {
        CompletableFuture<Customer> registeredCustomer = new CompletableFuture<>();

        // Create customer to register
        Customer customerToRegister = new Customer();
        customerToRegister.setBankId(new BankId("123"));

        // Wait for customer to be registered
        new Thread(() -> {
            var result = TestUtil.customerService.register(customerToRegister.getBankId());
            registeredCustomer.complete(result);
        }).start();

        // Check if right event has been sent
        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, CustomerRegistrationRequestedEvent.class).getCorrelationId();
        var expected = new CustomerRegistrationRequestedEvent(correlationId, customerToRegister.getBankId());
        assertEquals(expected, capturedEvent.getArgument(0, CustomerRegistrationRequestedEvent.class));

        // Check if handler handles
        Event event = new CustomerRegisteredEvent(correlationId, customerToRegister.getId(), customerToRegister.getBankId()).asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerRegisteredEvent.class);
        assertEquals(customerToRegister, registeredCustomer.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void successfulDeRegisterCustomerTest() {
        CompletableFuture<Void> result = new CompletableFuture<>();

        // Create merchant to Deregister
        Customer customerToDeRegister = new Customer();
        customerToDeRegister.setId(new Id("123"));

        // Wait for merchant to be registered
        new Thread(() -> {
            try {
                TestUtil.customerService.deregister(customerToDeRegister.getId());
                result.complete(null);
            } catch (Exception ignored) {
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, CustomerDeregistrationRequestedEvent.class).getCorrelationId();
        var expected = new CustomerDeregistrationRequestedEvent(correlationId, customerToDeRegister.getId());
        assertEquals(expected, capturedEvent.getArgument(0, CustomerDeregistrationRequestedEvent.class));

        Event event = new CustomerDeregisteredEvent(correlationId).asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerDeregisteredEvent.class);
        result.orTimeout(5, TimeUnit.SECONDS).join();
    }

    @Test
    public void failedDeregisterCustomerTest() {
        CompletableFuture<String> result = new CompletableFuture<>();

        // Create merchant to Deregister
        Customer customerToDeRegister = new Customer();
        customerToDeRegister.setId(new Id("123"));

        // Wait for merchant to be registered
        new Thread(() -> {
            try {
                TestUtil.customerService.deregister(customerToDeRegister.getId());
            } catch (Exception e) {
                result.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, CustomerDeregistrationRequestedEvent.class).getCorrelationId();
        var expected = new CustomerDeregistrationRequestedEvent(correlationId, customerToDeRegister.getId());
        assertEquals(expected, capturedEvent.getArgument(0, CustomerDeregistrationRequestedEvent.class));

        Event event = new CustomerNotDeregisteredEvent(correlationId, "error").asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerNotDeregisteredEvent.class);
        String error = result.orTimeout(5, TimeUnit.SECONDS).join();
        assertEquals("error", error);
    }

    @Test
    public void successfulGetReportCustomerTest() {
        CompletableFuture<CustomerReport> report = new CompletableFuture<>();

        new Thread(() -> {
            try {
                var result = TestUtil.customerService.getReport(new Id("123"));
                report.complete(result);
            } catch (Exception ignored) {}
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, CustomerReportRequestedEvent.class).getCorrelationId();
        var expected = new CustomerReportRequestedEvent(correlationId, new Id("123"));
        assertEquals(expected, capturedEvent.getArgument(0, CustomerReportRequestedEvent.class));

        Event event = new CustomerReportProcessedEvent(correlationId, new CustomerReport()).asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerReportProcessedEvent.class);
        assertNotNull(report.orTimeout(5, TimeUnit.SECONDS).join());
    }

    @Test
    public void failedGetReportCustomerTest() {
        CompletableFuture<String> errorMessage = new CompletableFuture<>();

        new Thread(() -> {
            try {
                TestUtil.customerService.getReport(new Id("123"));
            } catch (Exception e) {
                errorMessage.complete(e.getMessage());
            }
        }).start();

        Event capturedEvent = TestUtil.awaitEvent(capturedEvents);

        CorrelationId correlationId = capturedEvent.getArgument(0, CustomerReportRequestedEvent.class).getCorrelationId();
        var expected = new CustomerReportRequestedEvent(correlationId, new Id("123"));
        assertEquals(expected, capturedEvent.getArgument(0, CustomerReportRequestedEvent.class));

        Event event = new CustomerDidNotExistEvent(correlationId, null).asEvent();
        TestUtil.publishEvent(consumerMap, event, CustomerDidNotExistEvent.class);
        assertEquals("The customer is not registered in DTU Pay", errorMessage.orTimeout(5, TimeUnit.SECONDS).join());
    }
}
