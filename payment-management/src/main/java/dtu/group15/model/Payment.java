/** @author Aleksander */
package dtu.group15.model;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.events.internal.*;
import lombok.*;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

@Data
@AllArgsConstructor
public class Payment implements Serializable {
    private static final long serialVersionUID = 1L;
    private Id customerId;
    private Id merchantId;
    private Id paymentId;
    private BankId merchantBankId;
    private BankId customerBankId;
    private Amount amount;
    private Token token;
    private Boolean moneyTransferred;

    @Setter(AccessLevel.NONE)
    private List<DTUPayEvent> appliedEvents = new ArrayList<DTUPayEvent>();

    private Map<Class<? extends DTUPayEvent>, Consumer<DTUPayEvent>> handlers = new HashMap<>();

    //Constructor that adds all the handlers for the event bus in Event Store.
    public Payment() {
        registerEventHandlers();
    }

    private void registerEventHandlers() {
        handlers.put(PaymentCreatedEvent.class, e -> apply((PaymentCreatedEvent) e));
        handlers.put(UpdateTokenEvent.class, e -> apply((UpdateTokenEvent) e));
        handlers.put(UpdateCustomerIdEvent.class, e -> apply((UpdateCustomerIdEvent) e));
        handlers.put(UpdateMerchantIdEvent.class, e -> apply((UpdateMerchantIdEvent) e));
        handlers.put(UpdateCustomerBankIdEvent.class, e -> apply((UpdateCustomerBankIdEvent) e));
        handlers.put(UpdateMerchantBankIdEvent.class, e -> apply((UpdateMerchantBankIdEvent) e));
        handlers.put(UpdateAmountEvent.class, e -> apply((UpdateAmountEvent) e));
        handlers.put(UpdateMoneyTransferredEvent.class, e -> apply((UpdateMoneyTransferredEvent) e));
    }

    private static Id createPaymentID() {
        return new Id(UUID.randomUUID().toString());
    }

    //Creates and returns an empty payment.
    public static Payment create() {
        Id paymentId = Payment.createPaymentID();
        PaymentCreatedEvent event = new PaymentCreatedEvent(paymentId);
        Payment payment = new Payment();
        payment.setPaymentId(paymentId);
        payment.appliedEvents.add(event);
        return payment;
    }

    //Creates and return a payment with events applied.
    public static Payment createFromEvents(Stream<DTUPayEvent> events) {
        Payment payment = new Payment();
        payment.applyEvents(events);
        return payment;
    }


    /* Methods for updating a payment */

    public void updateToken(Token token) {
        DTUPayEvent event = new UpdateTokenEvent(paymentId, token);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateMerchantId(Id id) {
        DTUPayEvent event = new UpdateMerchantIdEvent(paymentId, id);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateCustomerId(Id id) {
        DTUPayEvent event = new UpdateCustomerIdEvent(paymentId, id);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateMerchantBankId(BankId bankId) {
        DTUPayEvent event = new UpdateMerchantBankIdEvent(paymentId, bankId);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateCustomerBankId(BankId bankId) {
        DTUPayEvent event = new UpdateCustomerBankIdEvent(paymentId, bankId);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateAmount(Amount amount) {
        DTUPayEvent event = new UpdateAmountEvent(paymentId, amount);
        appliedEvents.add(event);
        applyEvent(event);
    }

    public void updateMoneyTransferred(Boolean moneyTransferred) {
        DTUPayEvent event = new UpdateMoneyTransferredEvent(paymentId, moneyTransferred);
        appliedEvents.add(event);
        applyEvent(event);
    }

    //Applies events to the payment. Throws error if the payment is not recognized or if the handler for an event is missing.
    private void applyEvents(Stream<DTUPayEvent> events) throws Error {
        events.forEachOrdered(this::applyEvent);
        if (this.getPaymentId() == null) {
            throw new Error("payment does not exist");
        }
    }

    private void applyEvent(DTUPayEvent e) {
        handlers.getOrDefault(e.getClass(), this::missingHandler).accept(e);
    }

    private void missingHandler(DTUPayEvent e) {
        throw new Error("handler for event "+e+" missing");
    }


    /* Handlers for incoming internal events */

    private void apply(UpdateTokenEvent e) {
        this.token = e.getToken();
    }

    private void apply(PaymentCreatedEvent e) {
        this.paymentId = e.getPaymentId();
    }

    private void apply(UpdateCustomerIdEvent e) {
        this.customerId = e.getCustomerId();
    }

    private void apply(UpdateMerchantIdEvent e) {
        this.merchantId = e.getMerchantId();
    }

    private void apply(UpdateCustomerBankIdEvent e) {
        this.customerBankId = e.getCustomerBankId();
    }

    private void apply(UpdateMerchantBankIdEvent e) {
        this.merchantBankId = e.getMerchantBankId();
    }

    private void apply(UpdateAmountEvent e) {
        this.amount = e.getAmount();
    }

    private void apply(UpdateMoneyTransferredEvent e) {
        this.moneyTransferred = e.getMoneyTransferred();
    }

    public void clearAppliedEvents() {
        appliedEvents.clear();
    }

    public boolean isComplete() {
        return customerId != null && merchantId != null && paymentId != null && merchantBankId != null && customerBankId != null && amount != null && token != null && moneyTransferred != null && moneyTransferred;
    }

    public String toString() {
        return "Payment["+(paymentId == null ? null : paymentId.getId())+","+ customerId +","+ merchantId +","+merchantBankId+","+customerBankId+","+amount+","+token+","+moneyTransferred+"]";
    }
}
