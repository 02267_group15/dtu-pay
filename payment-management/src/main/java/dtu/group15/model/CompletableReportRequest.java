package dtu.group15.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompletableReportRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    Id accountId = null;
    Boolean exists = null;

    public boolean isComplete() {
        return accountId != null && exists != null;
    }
}

