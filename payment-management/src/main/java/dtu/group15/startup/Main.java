package dtu.group15.startup;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.adapter.RabbitMqInputPort;
import dtu.group15.adapter.RabbitMqOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.repository.PaymentRepository;
import dtu.group15.repository.ReadModelRepository;
import dtu.group15.service.PaymentService;
import dtu.ws.fastmoney.BankServiceService;
import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import messaging.implementations.RabbitMqQueue;

@QuarkusMain
public class Main {

    public static void main(String[] args) {
        Quarkus.run(StartUp.class, args);
    }

    public static class StartUp implements QuarkusApplication {
        @Override
        public int run(String... args) throws Exception {
            Logger.start();
            var mq = new RabbitMqQueue("rabbit-mq");
            var paymentRepository = new PaymentRepository(mq);
            var readModelRepository = new ReadModelRepository(mq);
            IOutputPort outputPort = new RabbitMqOutputPort(mq);
            PaymentService paymentService = new PaymentService(
                    paymentRepository,
                    readModelRepository,
                    outputPort,
                    new BankServiceService().getBankServicePort()
            );

            new RabbitMqInputPort(paymentService, mq);

            Quarkus.waitForExit();
            return 0;
        }
    }
}
