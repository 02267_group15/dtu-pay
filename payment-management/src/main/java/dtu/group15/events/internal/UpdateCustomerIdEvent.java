package dtu.group15.events.internal;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UpdateCustomerIdEvent extends DTUPayEvent {
    Id paymentId;
    Id customerId;
}
