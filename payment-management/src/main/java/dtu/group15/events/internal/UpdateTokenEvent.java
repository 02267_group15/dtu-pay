package dtu.group15.events.internal;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.Id;
import dtu.group15.model.Token;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UpdateTokenEvent extends DTUPayEvent {
    Id paymentId;
    Token token;
}
