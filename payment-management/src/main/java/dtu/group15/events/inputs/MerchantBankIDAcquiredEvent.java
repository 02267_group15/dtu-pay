package dtu.group15.events.inputs;


import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.BankId;
import dtu.group15.model.CorrelationId;
import dtu.group15.model.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MerchantBankIDAcquiredEvent extends DTUPayEvent {
    private static final long serialVersionUID = 1L;
    CorrelationId correlationId;
    Id merchantId;
    BankId merchantBankId;
}