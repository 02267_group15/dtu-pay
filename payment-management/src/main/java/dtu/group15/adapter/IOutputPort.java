/** @author Nicolai */
package dtu.group15.adapter;

import dtu.group15.model.*;

public interface IOutputPort {

    void createManagerReportProcessedEvent(CorrelationId correlationId, ManagerReport report);

    void createCustomerReportProcessedEvent(CorrelationId correlationId, CustomerReport report);

    void createMerchantReportProcessedEvent(CorrelationId corelationId, MerchantReport report);

    void paymentSucceeded(CorrelationId correlationId, Id merchantId, Token token, Amount amount);

    void paymentFailed(CorrelationId correlationId, String message);
}
