/** @author Michael */
package dtu.group15.adapter;

import dtu.group15.events.inputs.*;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import dtu.group15.service.PaymentService;
import messaging.MessageQueue;

public class RabbitMqInputPort {

    private final PaymentService paymentService;

    //Constructor that adds all the handlers for RabbitMQ
    public RabbitMqInputPort(PaymentService service, MessageQueue messageQueue) {
        this.paymentService = service;

        messageQueue.addHandler(CustomerBankIDAcquiredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerBankIDAcquiredEvent.class)));
        messageQueue.addHandler(MerchantBankIDAcquiredEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantBankIDAcquiredEvent.class)));
        messageQueue.addHandler(PaymentRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, PaymentRequestedEvent.class)));
        messageQueue.addHandler(ManagerReportRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, ManagerReportRequestedEvent.class)));
        messageQueue.addHandler(CustomerReportRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerReportRequestedEvent.class)));
        messageQueue.addHandler(MerchantReportRequestedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantReportRequestedEvent.class)));
        messageQueue.addHandler(MerchantExistedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, MerchantExistedEvent.class)));
        messageQueue.addHandler(CustomerExistedEvent.class.getSimpleName(), e -> this.apply(e.getArgument(0, CustomerExistedEvent.class)));
    }

    /* Handlers for incoming events */

    private void apply(CustomerBankIDAcquiredEvent e) {
        Logger.log(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        Id customerId = e.getCustomerId();
        BankId customerBankID = e.getCustomerBankId();
        this.paymentService.updatePaymentCustomerInfo(correlationId, customerId, customerBankID);

    }

    private void apply(MerchantBankIDAcquiredEvent e) {
        Logger.log(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        Id merchantId = e.getMerchantId();
        BankId merchantBankID = e.getMerchantBankId();
        this.paymentService.updatePaymentMerchantInfo(correlationId, merchantId, merchantBankID);
    }


    private void apply(PaymentRequestedEvent e) {
        Logger.newRequest(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        Amount amount = e.getAmount();
        Token token = e.getToken();
        this.paymentService.createPayment(correlationId, amount, token);
    }


    private void apply(ManagerReportRequestedEvent e) {
        Logger.newRequest(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        this.paymentService.getManagerReport(correlationId);
    }

    private void apply(CustomerReportRequestedEvent e) {
        Logger.newRequest(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();;
        Id customerID = e.getCustomerId();
        this.paymentService.customerReportRequest(correlationId, customerID);
    }


    private void apply(MerchantReportRequestedEvent e) {
        Logger.newRequest(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        Id merchantID = e.getMerchantId();
        this.paymentService.merchantReportRequest(correlationId, merchantID);
    }

    private void apply(CustomerExistedEvent e) {
        Logger.log(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();;
        this.paymentService.customerExists(correlationId);
    }


    private void apply(MerchantExistedEvent e) {
        Logger.log(e.getType() + " received " + e.getCorrelationId());
        CorrelationId correlationId = e.getCorrelationId();
        this.paymentService.merchantExists(correlationId);
    }

}
