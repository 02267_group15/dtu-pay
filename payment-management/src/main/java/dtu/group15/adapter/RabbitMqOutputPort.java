/** @author Nicolai */
package dtu.group15.adapter;

import dtu.group15.events.outputs.CustomerReportProcessedEvent;
import dtu.group15.events.outputs.ManagerReportProcessedEvent;
import dtu.group15.events.outputs.MerchantReportProcessedEvent;
import dtu.group15.events.outputs.*;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import messaging.MessageQueue;

public class RabbitMqOutputPort implements IOutputPort{

    private final MessageQueue messageQueue;

    public RabbitMqOutputPort(MessageQueue messageQueue) {
        this.messageQueue = messageQueue;
    }

    /* The methods used in this class are used to publish different events to the message queue */

    @Override
    public void createManagerReportProcessedEvent(CorrelationId correlationId, ManagerReport report) {
        Logger.log("ManagerReportProcessedEvent sent");
        messageQueue.publish(new ManagerReportProcessedEvent(correlationId, report).asEvent());
    }

    @Override
    public void createCustomerReportProcessedEvent(CorrelationId correlationId, CustomerReport report) {
        Logger.log("CustomerReportProcessedEvent sent");
        messageQueue.publish(new CustomerReportProcessedEvent(correlationId, report).asEvent());
    }

    @Override
    public void createMerchantReportProcessedEvent(CorrelationId correlationId, MerchantReport report) {
        Logger.log("MerchantReportProcessedEvent sent");
        messageQueue.publish(new MerchantReportProcessedEvent(correlationId, report).asEvent());
    }

    @Override
    public void paymentSucceeded(CorrelationId correlationId, Id merchantId, Token token, Amount amount) {
        Logger.log("PaymentProcessedEvent sent");
        messageQueue.publish(new PaymentProcessedEvent(correlationId, merchantId, token, amount).asEvent());
    }

    @Override
    public void paymentFailed(CorrelationId correlationId, String message) {
        Logger.log("PaymentFailedEvent sent");
        messageQueue.publish(new PaymentFailedEvent(correlationId, message).asEvent());
    }

}
