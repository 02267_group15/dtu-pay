/** @author Christian */
package dtu.group15.service;

import dtu.group15.adapter.IOutputPort;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import dtu.group15.repository.PaymentRepository;
import dtu.group15.repository.ReadModelRepository;
import dtu.ws.fastmoney.BankService;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PaymentService {

    private final BankService bank;
    private final PaymentRepository paymentRepository;
    private final ReadModelRepository readModelRepository;

    private final Map<CorrelationId, Id> pendingPayments = new ConcurrentHashMap<>();
    private final Map<CorrelationId, CompletableReportRequest> reportMerchantPreparation = new ConcurrentHashMap<>();
    private final Map<CorrelationId, CompletableReportRequest> reportCustomerPreparation = new ConcurrentHashMap<>();

    private final IOutputPort outputPort;

    public PaymentService(PaymentRepository paymentRepository, ReadModelRepository readModelRepository, IOutputPort outputPort, BankService bank) {
        this.bank = bank;
        this.paymentRepository = paymentRepository;
        this.readModelRepository = readModelRepository;
        this.outputPort = outputPort;
    }

    //Handles the payment requested event. Either creates or updates a payment.
    public void createPayment(CorrelationId correlationId, Amount amount, Token token) {
        Payment p = getPendingPayment(correlationId);
        p.updateAmount(amount);
        p.updateToken(token);
        paymentRepository.save(p);
        tryCompletePayment(correlationId);
    }

    //Handles the merchant bank ID acquired event. Updates the merchant ID and bank ID of a payment.
    public void updatePaymentMerchantInfo(CorrelationId correlationId, Id merchantID, BankId merchantBankID) {
        Payment p = getPendingPayment(correlationId);
        p.updateMerchantId(merchantID);
        p.updateMerchantBankId(merchantBankID);
        paymentRepository.save(p);
        tryCompletePayment(correlationId);
    }

    //Handles the customer bank ID acquired event. Updates the customer ID and bank ID of a payment.
    public void updatePaymentCustomerInfo(CorrelationId correlationId, Id customerId, BankId customerBankID) {
        Payment p = getPendingPayment(correlationId);
        p.updateCustomerId(customerId);
        p.updateCustomerBankId(customerBankID);
        paymentRepository.save(p);
        tryCompletePayment(correlationId);
    }

    //Completes a payment if it is ready.
    private synchronized void tryCompletePayment(CorrelationId correlationId) {
        if (!pendingPayments.containsKey(correlationId)) {
            //Solves concurrency problems.
            return;
        }

        Id id = pendingPayments.get(correlationId);
        Payment payment = paymentRepository.getById(id);

        if (payment.getMerchantBankId() != null && payment.getCustomerBankId() != null && payment.getAmount() != null) {
            this.pendingPayments.remove(correlationId);
            this.transferMoney(correlationId, payment);
        }
        else {
            Logger.log("Payment not done yet " + correlationId + " " + payment.getPaymentId());
        }
    }

    //Completes a customer report if it is ready.
    private synchronized void tryCompleteCustomerReport(CorrelationId correlationId) {
        if (!reportCustomerPreparation.containsKey(correlationId)) {
            return;
        }
        CompletableReportRequest completableReportRequest = reportCustomerPreparation.get(correlationId);
        if (completableReportRequest.isComplete()) {
            reportCustomerPreparation.remove(correlationId);
            getCustomerReport(correlationId, completableReportRequest.getAccountId());
        }
    }

    //Completes a merchant report if it is ready.
    private synchronized void tryCompleteMerchantReport(CorrelationId correlationId) {
        if (!reportMerchantPreparation.containsKey(correlationId)) {
            return;
        }
        CompletableReportRequest completableReportRequest = reportMerchantPreparation.get(correlationId);
        if (completableReportRequest.isComplete()) {
            reportMerchantPreparation.remove(correlationId);
            getMerchantReport(correlationId, completableReportRequest.getAccountId());
        }
    }

    //Requests bank to transfer money from customer to merchant.
    private void transferMoney(CorrelationId correlationId, Payment payment) {
        Logger.log("Transferring money " + correlationId + " " + payment.getPaymentId());
        try {
            bank.transferMoneyFromTo(payment.getCustomerBankId().getId(), payment.getMerchantBankId().getId(), new BigDecimal(payment.getAmount().getAmount()), "DTUPay");
            payment.updateMoneyTransferred(true);
            this.paymentRepository.save(payment);
            Logger.log("Transfer money succeeded " + correlationId + " " + payment.getPaymentId());
            this.outputPort.paymentSucceeded(correlationId, payment.getMerchantId(), payment.getToken(), payment.getAmount());
        } catch(Exception exception) {
            Logger.log("Transfer money failed " + correlationId + " " + payment.getPaymentId());
            payment.updateMoneyTransferred(false);
            this.paymentRepository.save(payment);
            this.outputPort.paymentFailed(correlationId, exception.getMessage());
        }
    }

    //Must be synchronized due to concurrency issues.
    private synchronized Payment getPendingPayment(CorrelationId correlationId) {
        if (pendingPayments.containsKey(correlationId)) {
            Id paymentId = pendingPayments.get(correlationId);
            return paymentRepository.getById(paymentId);
        }
        else {
            Payment payment = Payment.create();
            paymentRepository.save(payment);
            pendingPayments.put(correlationId, payment.getPaymentId());
            return payment;
        }
    }

    /* Queries */

    public void merchantExists(CorrelationId correlationId) {
        CompletableReportRequest completableReportRequest = getCompletableMerchantReport(correlationId);
        completableReportRequest.setExists(true);
        tryCompleteMerchantReport(correlationId);
    }

    public void customerExists(CorrelationId correlationId) {
        CompletableReportRequest completableReportRequest = getCompletableCustomerReport(correlationId);
        completableReportRequest.setExists(true);
        tryCompleteCustomerReport(correlationId);
    }

    public void merchantReportRequest(CorrelationId correlationId, Id accountId) {
        CompletableReportRequest completableReportRequest = getCompletableMerchantReport(correlationId);
        completableReportRequest.setAccountId(accountId);
        tryCompleteMerchantReport(correlationId);
    }

    public void customerReportRequest(CorrelationId correlationId, Id accountId) {
        CompletableReportRequest completableReportRequest = getCompletableCustomerReport(correlationId);
        completableReportRequest.setAccountId(accountId);
        tryCompleteCustomerReport(correlationId);
    }

    public void getManagerReport(CorrelationId correlationId) {
        ManagerReport report = this.readModelRepository.getManagerReport();
        this.outputPort.createManagerReportProcessedEvent(correlationId, report);
    }

    public void getCustomerReport(CorrelationId correlationId, Id customerID) {
        CustomerReport report = this.readModelRepository.getCustomerReport(customerID);
        this.outputPort.createCustomerReportProcessedEvent(correlationId, report);

    }

    public void getMerchantReport(CorrelationId correlationId, Id merchantID) {
        MerchantReport report = this.readModelRepository.getMerchantReport(merchantID);
        this.outputPort.createMerchantReportProcessedEvent(correlationId, report);
    }

    private synchronized CompletableReportRequest getCompletableCustomerReport(CorrelationId correlationId) {
        if (reportCustomerPreparation.containsKey(correlationId)) {
            return reportCustomerPreparation.get(correlationId);
        }
        else {
            CompletableReportRequest completableReportRequest = new CompletableReportRequest();
            reportCustomerPreparation.put(correlationId, completableReportRequest);
            return completableReportRequest;
        }
    }

    private synchronized CompletableReportRequest getCompletableMerchantReport(CorrelationId correlationId) {
        if (reportMerchantPreparation.containsKey(correlationId)) {
            return reportMerchantPreparation.get(correlationId);
        }
        else {
            CompletableReportRequest completableReportRequest = new CompletableReportRequest();
            reportMerchantPreparation.put(correlationId, completableReportRequest);
            return completableReportRequest;
        }
    }
}
