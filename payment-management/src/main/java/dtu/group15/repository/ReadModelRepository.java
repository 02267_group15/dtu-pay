/** @author Jens */
package dtu.group15.repository;

import dtu.group15.events.internal.*;
import dtu.group15.logger.Logger;
import dtu.group15.model.*;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReadModelRepository {

    private final Map<Id, Payment> payments = new ConcurrentHashMap<>();

    //Constructor that adds all the handlers for RabbitMQ
    public ReadModelRepository(MessageQueue messageQueue) {
        messageQueue.addHandler(PaymentCreatedEvent.class.getSimpleName(), e -> apply(e.getArgument(0, PaymentCreatedEvent.class)));
        messageQueue.addHandler(UpdateCustomerIdEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateCustomerIdEvent.class)));
        messageQueue.addHandler(UpdateMerchantIdEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateMerchantIdEvent.class)));
        messageQueue.addHandler(UpdateCustomerBankIdEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateCustomerBankIdEvent.class)));
        messageQueue.addHandler(UpdateMerchantBankIdEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateMerchantBankIdEvent.class)));
        messageQueue.addHandler(UpdateAmountEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateAmountEvent.class)));
        messageQueue.addHandler(UpdateMoneyTransferredEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateMoneyTransferredEvent.class)));
        messageQueue.addHandler(UpdateTokenEvent.class.getSimpleName(), e -> apply(e.getArgument(0, UpdateTokenEvent.class)));
    }


    /* Queries */

    public ManagerReport getManagerReport() {
        Logger.log("Getting manager report");
        List<Payment> paymentList = new ArrayList<>(payments.values());
        Integer totalSum = paymentList.stream()
                .filter(Payment::isComplete)
                .map(Payment::getAmount)
                .map(Amount::getAmount).reduce(Integer::sum).orElse(0);

        ManagerReport managerReport = new ManagerReport();
        Amount amount = new Amount(totalSum);
        managerReport.setTotalAmount(amount);

        List<ManagerReportedPayment> managerReportedPayments = new ArrayList<>();
        for (Payment payment : paymentList) {
            if(payment.isComplete()) {
                ManagerReportedPayment managerReportedPayment = new ManagerReportedPayment();
                managerReportedPayment.setPaymentId(payment.getPaymentId());
                managerReportedPayment.setCustomerId(payment.getCustomerId());
                managerReportedPayment.setMerchantId(payment.getMerchantId());
                managerReportedPayment.setAmount(payment.getAmount());
                managerReportedPayment.setToken(payment.getToken());
                managerReportedPayments.add(managerReportedPayment);
            }
        }

        managerReport.setPayments(managerReportedPayments);
        return managerReport;
    }

    public CustomerReport getCustomerReport(Id customerID) {
        Logger.log("Getting customer report");
        List<Payment> paymentList = new ArrayList<>(payments.values());
        List<CustomerReportedPayment> result = new ArrayList<>();
        for (Payment payment : paymentList) {
            if (payment.isComplete() &&
                    payment.getCustomerId().equals(customerID)) {
                CustomerReportedPayment customerReportedPayment = new CustomerReportedPayment();
                customerReportedPayment.setMerchantId(payment.getMerchantId());
                customerReportedPayment.setAmount(payment.getAmount());
                customerReportedPayment.setToken(payment.getToken());
                result.add(customerReportedPayment);
            }
        }
        return new CustomerReport(result);
    }

    public MerchantReport getMerchantReport(Id merchantID) {
        Logger.log("Getting merchant report");
        List<Payment> paymentList = new ArrayList<>(payments.values());
        List<MerchantReportedPayment> result = new ArrayList<>();
        for (Payment payment : paymentList) {
            if (payment.isComplete() &&
                    payment.getMerchantId().equals(merchantID)) {
                MerchantReportedPayment merchantReportedPayment = new MerchantReportedPayment();
                merchantReportedPayment.setAmount(payment.getAmount());
                merchantReportedPayment.setToken(payment.getToken());
                result.add(merchantReportedPayment);
            }
        }
        return new MerchantReport(result);
    }


    /* Handlers for incoming events */

    private void apply(PaymentCreatedEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setPaymentId(e.getPaymentId());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateCustomerIdEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setCustomerId(e.getCustomerId());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateMerchantIdEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setMerchantId(e.getMerchantId());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateCustomerBankIdEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setCustomerBankId(e.getCustomerBankId());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateMerchantBankIdEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setMerchantBankId(e.getMerchantBankId());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateAmountEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setAmount(e.getAmount());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateMoneyTransferredEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setMoneyTransferred(e.getMoneyTransferred());
        Logger.log(e.getType() + " out " + p);
    }

    private void apply(UpdateTokenEvent e) {
        Logger.log(e.getType() + " in " + e.getPaymentId().getId());
        Payment p = getOrCreatePayment(e.getPaymentId());
        p.setToken(e.getToken());
        Logger.log(e.getType() + " out " + p);
    }

    //Creates payment if it does not already exists, otherwise gets the payment.
    private synchronized Payment getOrCreatePayment(Id paymentId) {
        if (payments.containsKey(paymentId)) {
            return payments.get(paymentId);
        }
        Payment p = new Payment();
        payments.put(paymentId, p);
        return p;
    }
}
