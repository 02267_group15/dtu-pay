/** @author Michael */
package dtu.group15.repository;

import dtu.group15.model.*;
import messaging.MessageQueue;

public class PaymentRepository {
    private final EventStore eventStore;

    public PaymentRepository(MessageQueue queue) {
        this.eventStore = new EventStore(queue);
    }

    //Return the payment with ID id by applying saved events.
    public synchronized Payment getById(Id id) {
        return Payment.createFromEvents(eventStore.getEventsFor(id));
    }

    //Saves a payments and saves the events applied to the payment.
    public synchronized void save(Payment payment) {
        eventStore.addEvents(payment.getPaymentId(), payment.getAppliedEvents());
        payment.clearAppliedEvents();
    }
}
