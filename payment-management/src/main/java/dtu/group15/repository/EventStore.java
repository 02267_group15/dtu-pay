/** @author Nicolai */
package dtu.group15.repository;

import dtu.group15.events.DTUPayEvent;
import dtu.group15.model.Id;
import lombok.NonNull;
import messaging.Event;
import messaging.MessageQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public class EventStore {

    /*
    This class stores events and provides methods for getting and adding stored events.
     */

    private Map<Id, List<DTUPayEvent>> store = new ConcurrentHashMap<>();

    private MessageQueue eventBus;

    public EventStore(MessageQueue bus) {
        this.eventBus = bus;
    }

    public void addEvent(Id id, DTUPayEvent event) {
        if (!store.containsKey(id)) {
            store.put(id, new ArrayList<DTUPayEvent>());
        }
        store.get(id).add(event);
        eventBus.publish(event.asEvent());
    }

    public Stream<DTUPayEvent> getEventsFor(Id id) {
        if (!store.containsKey(id)) {
            store.put(id, new ArrayList<DTUPayEvent>());
        }
        return store.get(id).stream();
    }

    public void addEvents(@NonNull Id id, List<DTUPayEvent> appliedEvents) {
        appliedEvents.stream().forEach(e -> addEvent(id, e));
    }
}
