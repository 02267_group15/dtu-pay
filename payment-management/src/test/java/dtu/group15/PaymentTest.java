/** @author Michael */
package dtu.group15;

import dtu.group15.adapter.RabbitMqInputPort;
import dtu.group15.adapter.RabbitMqOutputPort;
import dtu.group15.events.inputs.*;
import dtu.group15.events.outputs.*;
import dtu.group15.model.*;
import dtu.group15.repository.PaymentRepository;
import dtu.group15.repository.ReadModelRepository;
import dtu.group15.service.PaymentService;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException;
import dtu.ws.fastmoney.BankServiceException_Exception;
import messaging.Event;
import messaging.MessageQueue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.UUID;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class PaymentTest {
    private Map<String, List<Consumer<Event>>> consumerMap;
    private MessageQueue queue;
    private List<Event> capturedEvents;
    private CorrelationId correlationId;
    private PaymentRepository paymentRepository;
    private ReadModelRepository readModelRepository;
    private RabbitMqOutputPort outputPort;
    private PaymentService service;
    private BankService bank;

    @Before
    public void setupEventHandlingAndServiceBankTransferSuccess() throws BankServiceException_Exception {
        bank = mock(BankService.class);
        doNothing().when(bank).transferMoneyFromTo(Mockito.anyString(), Mockito.anyString(), Mockito.any(), Mockito.anyString());

        queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue, consumerMap);
        correlationId = new CorrelationId(UUID.randomUUID().toString());
        paymentRepository = new PaymentRepository(queue);
        readModelRepository = new ReadModelRepository(queue);
        outputPort = new RabbitMqOutputPort(queue);
        service = new PaymentService(paymentRepository, readModelRepository, outputPort, bank);
        new RabbitMqInputPort(service, queue);
    }

    @Test
    public void testPaymentRequestSuccessFull() {
        // Create payment requested
        TestUtil.publishEvent(consumerMap,
                new PaymentRequestedEvent(correlationId, new Id(), new Token(), new Amount()).asEvent(),
                PaymentRequestedEvent.class
        );

        // BankIdAcquired
        TestUtil.publishEvent(consumerMap,
                new MerchantBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                MerchantBankIDAcquiredEvent.class
        );

        // Customer Bank Id acquired
        TestUtil.publishEvent(consumerMap,
                new CustomerBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                CustomerBankIDAcquiredEvent.class
        );

        Event event = capturedEvents.getLast();
        assertNotNull(event);
        assertNotNull(event.getArgument(0, PaymentProcessedEvent.class));

    }

    @Test
    public void testPaymentRequestFailed() throws BankServiceException_Exception {

        // Test setup
        bank = mock(BankService.class);
        doThrow(new BankServiceException_Exception("Bank transfer failed", new BankServiceException()))
                .when(bank).transferMoneyFromTo(Mockito.anyString(), Mockito.anyString(), Mockito.any(), Mockito.anyString());

        queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue, consumerMap);
        correlationId = new CorrelationId(UUID.randomUUID().toString());
        paymentRepository = new PaymentRepository(queue);
        readModelRepository = new ReadModelRepository(queue);
        outputPort = new RabbitMqOutputPort(queue);
        service = new PaymentService(paymentRepository, readModelRepository, outputPort, bank);
        new RabbitMqInputPort(service, queue);

        // Create payment requested
        TestUtil.publishEvent(consumerMap,
                new PaymentRequestedEvent(correlationId, new Id(), new Token(), new Amount()).asEvent(),
                PaymentRequestedEvent.class
        );

        // BankIdAcquired
        TestUtil.publishEvent(consumerMap,
                new MerchantBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                MerchantBankIDAcquiredEvent.class
        );

        // Customer Bank Id acquired
        TestUtil.publishEvent(consumerMap,
                new CustomerBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                CustomerBankIDAcquiredEvent.class
        );

        Event event = capturedEvents.getLast();
        assertNotNull(event.getArgument(0, PaymentFailedEvent.class));

    }

    @Test
    public void testPaymentRequestFailedNoMerchant() {
        // Create payment requested
        TestUtil.publishEvent(consumerMap,
                new PaymentRequestedEvent(correlationId, new Id(), new Token(), new Amount()).asEvent(),
                PaymentRequestedEvent.class
        );

        // Customer Bank Id acquired
        TestUtil.publishEvent(consumerMap,
                new CustomerBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                CustomerBankIDAcquiredEvent.class
        );

        assertNull(TestUtil.getEventByType(capturedEvents, PaymentProcessedEvent.class.getSimpleName()));
    }

    @Test
    public void testPaymentRequestFailedNoCustomer() {
        // Create payment requested
        TestUtil.publishEvent(consumerMap,
                new PaymentRequestedEvent(correlationId, new Id(), new Token(), new Amount()).asEvent(),
                PaymentRequestedEvent.class
        );

        // BankIdAcquired
        TestUtil.publishEvent(consumerMap,
                new MerchantBankIDAcquiredEvent(correlationId, new Id(), new BankId()).asEvent(),
                MerchantBankIDAcquiredEvent.class
        );

        assertNull(TestUtil.getEventByType(capturedEvents, PaymentProcessedEvent.class.getSimpleName()));
    }

    @Test
    public void paymentProcessedSuccessfullyTest() {
        performPayment();
    }

    @Test
    public void paymentProcessedFailedTest() throws BankServiceException_Exception {
        // Test setup
        bank = mock(BankService.class);
        doThrow(new BankServiceException_Exception("Bank transfer failed", new BankServiceException()))
                .when(bank).transferMoneyFromTo(Mockito.anyString(), Mockito.anyString(), Mockito.any(), Mockito.anyString());

        queue = mock(MessageQueue.class);
        consumerMap = TestUtil.mapConsumersToTopics(queue);
        capturedEvents = TestUtil.initializeEventPuller(queue, consumerMap);
        correlationId = new CorrelationId(UUID.randomUUID().toString());
        paymentRepository = new PaymentRepository(queue);
        readModelRepository = new ReadModelRepository(queue);
        outputPort = new RabbitMqOutputPort(queue);
        service = new PaymentService(paymentRepository, readModelRepository, outputPort, bank);
        new RabbitMqInputPort(service, queue);

        // Payment requested
        Event paymentRequestedEvent = new PaymentRequestedEvent(
                correlationId, new Id("Test Merchant ID"), new Token("Test Token"), new Amount(100)
        ).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        // Merchant bank ID acquired
        Event merchantBankIDAcquiredEvent = new MerchantBankIDAcquiredEvent(
                correlationId,
                new Id("Test Merchant ID"),
                new BankId("Test Merchant Bank ID")
        ).asEvent();
        TestUtil.publishEvent(consumerMap, merchantBankIDAcquiredEvent, MerchantBankIDAcquiredEvent.class);

        // Customer bank ID acquired
        Event customerBankIDAcquiredEvent = new CustomerBankIDAcquiredEvent(
                correlationId,
                new Id("Test Customer ID"),
                new BankId("Test Customer Bank ID")
        ).asEvent();
        TestUtil.publishEvent(consumerMap, customerBankIDAcquiredEvent, CustomerBankIDAcquiredEvent.class);

        // Payment processed
        Event expected = new PaymentFailedEvent(
                correlationId,
                "Bank transfer failed"
        ).asEvent();

        assertTrue(capturedEvents.contains(expected));
    }

    @Test
    public void customerReportRequestedTest() {
        performPayment();

        // Customer report requested
        Event customerReportRequestedEvent = new CustomerReportRequestedEvent(correlationId, new Id("Test Customer ID")).asEvent();
        TestUtil.publishEvent(consumerMap, customerReportRequestedEvent, CustomerReportRequestedEvent.class);

        // Customer existed
        Event customerExistedEvent = new CustomerExistedEvent(correlationId).asEvent();
        TestUtil.publishEvent(consumerMap, customerExistedEvent, CustomerExistedEvent.class);

        // Customer report processed
        CustomerReportedPayment expectedCustomerReportedPayment = new CustomerReportedPayment(new Id("Test Merchant ID"), new Token("Test Token"), new Amount(100));
        CustomerReport expectedCustomerReport = new CustomerReport(new ArrayList<>(){
            { add(expectedCustomerReportedPayment); }
        });
        Event expected = new CustomerReportProcessedEvent(correlationId, expectedCustomerReport).asEvent();
        assertTrue(capturedEvents.contains(expected));
    }

    @Test
    public void merchantReportRequestedTest() {
        performPayment();

        // Merchant report requested
        Event merchantReportRequestedEvent = new MerchantReportRequestedEvent(correlationId, new Id("Test Merchant ID")).asEvent();
        TestUtil.publishEvent(consumerMap, merchantReportRequestedEvent, MerchantReportRequestedEvent.class);

        // Merchant existed
        Event merchantExistedEvent = new MerchantExistedEvent(correlationId).asEvent();
        TestUtil.publishEvent(consumerMap, merchantExistedEvent, MerchantExistedEvent.class);

        // Merchant report processed
        MerchantReportedPayment expectedMerchantReportedPayment = new MerchantReportedPayment(new Token("Test Token"), new Amount(100));
        MerchantReport expectedMerchantReport = new MerchantReport(new ArrayList<>(){
            { add(expectedMerchantReportedPayment); }
        });
        Event expected = new MerchantReportProcessedEvent(correlationId, expectedMerchantReport).asEvent();
        assertTrue(capturedEvents.contains(expected));
    }

    @Test
    public void managerReportRequestedTest() {
        performPayment();

        // Manager report requested
        Event managerReportRequestedEvent = new ManagerReportRequestedEvent(correlationId).asEvent();
        TestUtil.publishEvent(consumerMap, managerReportRequestedEvent, ManagerReportRequestedEvent.class);

        // Manager report processed
        Event managerReportProcessedEvent = TestUtil.getEventByType(capturedEvents, ManagerReportProcessedEvent.class.getSimpleName());

        ManagerReportedPayment expectedPayment = new ManagerReportedPayment();
        expectedPayment.setPaymentId(managerReportProcessedEvent.getArgument(0, ManagerReportProcessedEvent.class).getManagerReport().getPayments().getFirst().getPaymentId());
        expectedPayment.setCustomerId(new Id("Test Customer ID"));
        expectedPayment.setMerchantId(new Id("Test Merchant ID"));
        expectedPayment.setAmount(new Amount(100));
        expectedPayment.setToken(new Token("Test Token"));

        ManagerReport expectedManagerReport = new ManagerReport(new Amount(100), new ArrayList<>() {
            { add(expectedPayment); }
        });
        Event expected = new ManagerReportProcessedEvent(correlationId, expectedManagerReport).asEvent();
        assertEquals(expected, managerReportProcessedEvent);
    }

    public void performPayment() {
        // Payment requested
        Event paymentRequestedEvent = new PaymentRequestedEvent(
                correlationId, new Id("Test Merchant ID"), new Token("Test Token"), new Amount(100)
        ).asEvent();
        TestUtil.publishEvent(consumerMap, paymentRequestedEvent, PaymentRequestedEvent.class);

        // Merchant bank ID acquired
        Event merchantBankIDAcquiredEvent = new MerchantBankIDAcquiredEvent(
                correlationId, new Id("Test Merchant ID"), new BankId("Test Merchant Bank ID")
        ).asEvent();
        TestUtil.publishEvent(consumerMap, merchantBankIDAcquiredEvent, MerchantBankIDAcquiredEvent.class);

        // Customer bank ID acquired
        Event customerBankIDAcquiredEvent = new CustomerBankIDAcquiredEvent(
                correlationId, new Id("Test Customer ID"), new BankId("Test Customer Bank ID")
        ).asEvent();
        TestUtil.publishEvent(consumerMap, customerBankIDAcquiredEvent, CustomerBankIDAcquiredEvent.class);

        // Payment processed
        Event expected = new PaymentProcessedEvent(
                correlationId, new Id("Test Merchant ID"), new Token("Test Token"), new Amount(100)
        ).asEvent();

        assertTrue(capturedEvents.contains(expected));
    }

}
