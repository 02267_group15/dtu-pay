/** @author Nicolai */
package dtu.group15;

import messaging.Event;
import messaging.MessageQueue;
import org.mockito.Mockito;

import java.util.*;
import java.util.function.Consumer;

public class TestUtil {
    public static Map<String, List<Consumer<Event>>> mapConsumersToTopics(MessageQueue queue) {
        Map<String, List<Consumer<Event>>> consumerMap = new HashMap<>();
        Mockito.doAnswer(invocation -> {
            String topic = invocation.getArgument(0);
            Consumer<Event> consumer = invocation.getArgument(1);
            if (consumerMap.containsKey(topic)) {
                consumerMap.get(topic).add(consumer);
            } else {
                consumerMap.put(topic, new ArrayList<>() {{ add(consumer); }});
            }
            return null;
        }).when(queue).addHandler(Mockito.anyString(), Mockito.any());
        return consumerMap;
    }

    public static List<Event> initializeEventPuller(MessageQueue queue, Map<String, List<Consumer<Event>>> consumerMap) {
        List<Event> capturedEvents = new ArrayList<>();
        Mockito.doAnswer(invocation -> {
            Event event = invocation.getArgument(0);
            capturedEvents.add(event);
            if(consumerMap.containsKey(event.getType())) {
                consumerMap.get(event.getType()).forEach(consumer -> consumer.accept(event));
            }
            return null;
        }).when(queue).publish(Mockito.any());

        return capturedEvents;
    }

    public static void publishEvent(Map<String, List<Consumer<Event>>> consumerMap, Event event, Class c) {
        consumerMap.get(c.getSimpleName()).forEach(consumer -> consumer.accept(event));
    }

    public static Event getEventByType(List<Event> events, String type) {
        return events.stream().filter(e -> e.getType().equals(type)).findAny().orElse(null);
    }
}
